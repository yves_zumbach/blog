---
title: Fighting Inflation and Screwing Future Generations
abstract: I think some measures to fight inflation caused by higher energy prices benefit current generation at the cost of future generation. Here is how I reach this conclusion and why I think it is bad.
category: economy
coverImage: /images/2022-11-18_madison-kaminski-5fd2jdyGo-8-unsplash.jpg
comment:
- DO NOT INDENT TEXT! It causes parsing errors.
---

People in Europe are going through inflation rates not seen in decades.
States are trying to minimize the effects on the population through various means like price caps.
I think these measures benefit the present generation at the cost of future generation, here is why.

# Physics

{% annotation %}
Note that comfort is not the same as life quality.
I think we have focuses way too much on comfort and not barely enough on life quality.
{% /annotation %}

Let's first take a quick step back and talk about physics.
Energy, measured in Joule, is the fundamental physical unit of action.
If you have more energy you can do more things.
Assume it becomes harder to get your hand on energy, then you are able to do less.
It is really that simple.
Less energy implies a simpler life, and less comfort.

{% page %}
  ![Light emitted by cities on Earth as seen from space](/images/2022-11-21_energy.jpg)
{% /page %}

# Causes of Inflation

Inflation can have many causes, and the current inflation crisis likely has multiple causes: supply-chain disturbances because of the war in Ukraine, dollar inflation because the American Federal Reserve injected 5 *trillion* into the American economy, and rising energy prices.
Let's focus on the energy prices.

In Europe, it has become harder to get your hand on energy.
One of the many reasons for this is that Russia is sending a lot less gas to Europe since Europe took measures against Russia because of the war it started with Ukraine.
But there are other reasons: in France, half of the nuclear reactors are stopped because of technical deficiencies or regular maintenance works.
In Switzerland, because it rained so little during the summer, the dams are not as full as they should be so we might be short on hydroelectrical power which makes 60% of our mix.
Throughout Europe we are also still paying the price of Corona: energy suppliers had to lower their production during the pandemic and are not all back to pre-pandemic production levels.
So it is harder to obtain energy in Europe, and therefor energy prices rose.

# A Little Paradox

Energy is the fundamental currency of everything, so if energy prices are going up, all other prices must go up too.
Measures to preserve purchasing power, like price caps on electricity introduced by the French government, seem nonsensical to me.
We are trying to cheat the laws of nature: we want to do as many things as when there was more energy.
Surprisingly, it seems to work...
There is a lot of inflation in Europe, some 20% compared to last year for some Scandinavian countries, yet inflation in France is the *lowest* in Europe at 7% only.
How is this possible?
Can we now cheat the fundamental laws of nature?

{% page %}
{% figure %}
![Statistic: Harmonized index of consumer prices (HICP) inflation rate of the European Union in October 2022, by country | Statista](https://www.statista.com/graphic/1/225698/monthly-inflation-rate-in-eu-countries.jpg)
{% figure-caption %}
Inflation rates in Europe in October 22 from [Statista](https://www.statista.com/statistics/225698/monthly-inflation-rate-in-eu-countries/)
{% /figure-caption %}
{% /figure %}
{% /page %}

# Consume Now, Pay Tomorrow

{% annotation %}
  When a country has a debt equal to 100% of BIP, it means that the country would have to dedicate all the value produced in the country during an entire year to pay back its debt.
{% /annotation %}
{% annotation %}
  Because of its national debt, the French people is paying more than 1700€ of interest *every second*, which amounts to 52 billions euros per year.
{% /annotation %}

I don't think we can cheat the laws of nature.
I think we are *borrowing from the future*, i.e. consuming today what we will only be able to afford tomorrow, by contracting debt.
And France has a debt worth [more than 100% of its BIP](https://www.statista.com/statistics/225698/monthly-inflation-rate-in-eu-countries/).
I think it is fair to see this as a transfer of value from future generations, i.e. those that will have to pay the debt back, to the current generation which is able to maintain its lifestyle today.
As part of the "younger" generation, I don't find this fair at all.
People today should not live at the expense of the people of tomorrow.

{% quote %}
No free meal.
{% /quote %}

# The Providence State

This problem is further fueled by the "providence state" narrative; this idea that a state must offer its citizen enough opportunities for a confortable life.
And the notion of "confortable life" has expanded massively during the 20{% sup %}th{% /sup %} century.
It went from having a heated home and enough food to having a car, meat at every meal, exotic fruits in all season, and going for holidays on the other side of the planet.
In the modern, "civilized" world, we should all be entitle to a lot of comfort, or so goes the narrative.
I think this is human hubris.
And we will have to pay the consequences at some point.

