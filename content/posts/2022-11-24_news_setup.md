---
title: My News Setup
abstract:
  Reading the news is a very natural way to extend my culture and to ensure my vision of the world remains consistent with reality.
  To achieve these goals without wasting too much time and without depressing myself, I had to be very conscious about how I read the news.
category: philosophy
coverImage: /images/2022-11-25_news.jpg
comment:
- DO NOT INDENT TEXT! It causes parsing errors.
---

*I've decided to improve on how I read news by being careful and conscious about where I get them from and how I am notified about them.*

# Goals and Dangers of Reading the News

News have the advantages of being limited in size and being renewed often enough that I can never run out.
They can be a nice way to entertain a take a little break from work.
Yet I need to be conscious about them, especially today when recommendation algorithms make it so easy to fall for the reinforcement bias.

{% annotation %}
- Be entertained.
- Ensure vision of the world remains consistent with reality.
{% /annotation %}
Right, so here are the goals that I want to achieve by reading the news.
First I want to be entertained.
Reading the news should confront me with varied opinions and perspectives so that I am forced to reflect, empathize with all viewpoints and build my own opinion.
This means that I should get liberal and conservative opinions; social and capitalist ones; from the left and from the right.
I want to ensure that my view of the world remain consistent with reality.

{% annotation %}
- Learn new languages.
{% /annotation %}
I am a native french-speaking Swiss citizen.
I use English pretty often.
I learned German in school, but I don't have so many opportunities to practice it, so I try not to loose too much of it by reading/listening/watching content in German.
I want my news setup to help me maintain my passive language skills, so I tend to look for news outlet in various languages.

{% annotation %}
- Enlarge my culture of the world by discovering news perspective, new cultures, new places.
{% /annotation %}
A tradeoff is generally made between how local a piece of news is and how important it is.
This means that news outlets will often include news that are not so important, but local, and some non-local but important news.
I have a strong preference for important news, and I don't really care about local ones.

{% annotation %}
- Avoid getting only negative and depressing news.
- Avoid notification overload.
{% /annotation %}
{% annotation %}
I recommend reading the following article for more on the topic of attention and the issues caused by phones and reading the news too much: [My awakening moment about how smartphones fragment our attention span](https://idratherbewriting.com/blog/awakening-moment-to-how-smartphones-fragment-our-attention/).
{% /annotation %}
Reading news can cause problems.
It can take too much time and prevent me from *doing*, so I need to be conscious about the quantity I want to read and how often I am reminded about the news.
Human brain biases drives us to click on negative and short articles.
Short articles are generally not very informative.
Negative articles, those that only talk about the problems of the world or that have a depressed or angry tone, drive a negative feeling in me which I want to avoid.

When it comes to news, there are many outlets that are not free.
I believe that the cost of having a perception of reality that is wrong, or of wasting my time on useless or suboptimal news *far outweighs* the cost of any news outlet out there.
For this reason, price is *not* a consideration when choosing news outlet for me.

# Previous News Setup

Previously and for no particular reasons, my news came from:

- BBC
- SRF news, the official news from the German-speaking part of Switzerland

This helped my practice German, and meant that I was getting news from Switzerland and the world (through a British point of view I guess).
However, the BBC has a very dramatic and negative tone; it felt like they were trying to hack my brain into clicking on their content.
Both BBC and SRF news sent me notifications for daily news, i.e. a lot of noise, not so much signal.
My goal is not to know about the latest fashionable piece of news to boost my ego when discussing with people, my goal is to learn about the world.
So in the end, I would just read the outline on the notification I received on my phone and discard the notification.

# First News Revamp

I was failing at so many of the goals I described above that I had to do a complete revamp of my news system.
In the end, I subscribed to the following news outlets:

- Republik.ch: German speaking, long format articles, focused mostly on Switzerland.
- Le Courrier International: French, aggregates articles from news outlets all over the world.

For both newspaper, I only subscribed to a single weekly newsletters.
Republik.ch has a nice newsletter sent every Friday on what important things happened during the week.
Regarding Le Courrier International, I read the weekly newspaper as a PDF on my computer. 
For both outlets, I only received a single mail newsletter per week.

# Second News Revamp

Unfortunately, after applying the recommendations in the article [*My awakening moment about how smartphones fragment our attention span*](https://idratherbewriting.com/blog/awakening-moment-to-how-smartphones-fragment-our-attention/), I started to be less and less addicted to reading the news.
This meant that I never read both Republik and Le Courrier International before the end of the week; it was too much content.
I had to make a choice between the two.
I valued the international and varied news from Le Courrier International more than getting German content, so I canceled my Republik subscription.

One of the goals I had by choosing written news instead of video-based ones was to have a more quite, less hectic life.
But it is often the case that I what to watch/read the news when I am cooking.
Reading on my computer while cooking is not possible, yet listening to video-based news and occasionally watching my screen when cooking is possible.
So I introduced some video news:

- Vice News, a Youtube channel: they have some of the most incredible reporters and always tackle highly interesting yet unheard of topics.
- 10vor10: the daily evening news from the German-speaking Switzerland, high-quality, many international news, in German and Swiss-German which is even better as I want to learn this language.

# Closing Thoughts

Some of the news outlets I have not mentioned above:

- Hackernews: I read a few articles from the *Best* section of the weekly newsletter.
  Those content are focused more on tech, though they often include philosophical, political, design-oriented content too.
- A few newsletters, though I make a very conscious effort to subscribe only a few of them (and unsubscribe whenever I have too many mails).

As I have very few notifications (a few emails per week), I have more time to actually *do* things.
I enjoy listening to news in German as a way to improve my language skills, but mostly I enjoy the diversity of the topics I am exposed to.
By receiving higher quality content overall, I think that I am more positive about life (which is not so easy in a time of global warming).
