---
title: Blue Zones
abstract: A study about the places in the world with the most centenaries, the so-called blue zones, found nine common characteristics that improve life expectancy, all of them rather easy to apply.
category: philosophy
coverImage: /images/2023-03-10_ikaria.jpg
comment:
- DO NOT INDENT TEXT! It causes parsing errors.
---

Blue zones are places on Earth where people live extraordinarily long *and well*, indicating that lifestyle probably has a large impact on longevity.
A study was conducted to find common patterns across all five identified blue zones.
This article is a compilation of the most interesting facts I read about in the article called [Blue Zones: Lessons from the World's Longest Lived](https://pubmed.ncbi.nlm.nih.gov/30202288/).

{% page %}
{% figure %}
![Five blue zones](/images/2023-03-10_blue-zones.webp)
{% figure-caption %}
The five identified Blue Zones.
{% /figure-caption %}
{% /figure %}
{% /page %}

20% of how many years we live is dictated by our genes.
The remaining 80% are dictated by our lifestyle.
So lifestyle is important.

There are nine identified characteristics shared by all five blue zones which are believed to increase lifespan – the power 9 – that I regrouped into eight bullets:

1. Move naturally: no iron pumping, no marathon running, instead live in an environment that always nudges you towards moving a bit (like growing a garden).
1. Know why you wake up in the morning.
   People in Nicoya, Costa Rica call it *plan de vida*, people on Okinawa, Japan call it *Ikigai*.
   This is worth up to seven years of life expectancy.
1. Have a routine to shed stress away every day.
   For example napping or yoga.
1. Eat better.
   *Hara hachi bu* is a Confusian principle to stop eating when you are 80% full.
   Also eat your last meal in the late afternoon and make it the smallest of the day.
   Eat mostly plants, meat is eaten 5 times per month, so approximately once per week.
1. Drink a bit of alcohol often.
   In almost all blue zones, alcohol is drunk in small quantities and often.
   Moderate drinkers outlive nondrinkers.
1. Have faith.
   Almost all centenarians in blue zones belong to a faith-based community.
1. Put your family first, e.g. keep your aging parents at home, commit to a life partner, invest time with your children.
1. Belong to a tribe with positive influence: it has been proven that smoking, obesity, happiness and loneliness are all contagious.

On a personal note, reading the article again is a good reminder that there are a couple of things that I need to change:

- Hack my environment and my activities to include more ways to move naturally.
  Sailing, in this regard, is a nice activity to learn.
- Routine to shed stress away: I want to start stretching every day.
- Napping more frequently: also a great way to lower stress as I need to release the tension in order to be able to sleep.
- Check that I do not have nutritious deficiencies because of my vegetarian diet (without going bankrupt if possible!).
- Find a purpose in life, or maybe start by defining what I want to do from next September on.
- Stop eating processed food altogether: cooking is great.
- Start drinking?
  I don't drink at all at the moment...
  But when I suggested that I wanted to start drinking to my doctor, he had a weird look on his face, lol.
- Become religious one way or another.
  Not sure how I will ever be able to achieve this though: my whole life is centered around being as rational as possible.
  Does being pastafarian really count?
  Somehow I doubt that it brings the health benefit I wish for.

