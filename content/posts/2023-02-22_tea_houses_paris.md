---
title: Tea Houses in Paris
abstract: Paris offers many tea houses, but not all tea houses are equal. Here are my favorite places.
category: tea
coverImage: /images/2023-02-22_maison_trois_thes.jpg
comment:
- DO NOT INDENT TEXT! It causes parsing errors.
---

Paris has a great diversity of tea houses, yet not all tea houses are equal.
Here are my favorite tea houses.

{% page %}
{% figure %}
![Maison des Trois Thés](/images/2023-02-22_maison_des_trois_thes_outside.jpg)
{% figure-caption %}
Maison des Trois Thés
{% /figure-caption %}
{% /figure %}
{% /page %}

The absolute best place to buy tea and tea gear is [La Maison des Trois Thés](https://www.openstreetmap.org/node/1691823176).
Don't look for a website, they don't have any.
They only sell manually processed, non-flavored teas.
They are specialized in Pu-Erh teas, but their collection contains more than a thousand different teas, so whatever you are looking for, you will most surely find it.

{% page %}
{% figure %}
![Logo du Conservatoire des Hémisphères](/images/2023-02-22_conservatoire_des_hemisphere.webp)
{% figure-caption %}
Conservatoire des Hémisphères
{% /figure-caption %}
{% /figure %}
{% /page %}

One other shop that I like a lot is [Le Conservatoire des Hémisphères](https://hemispheresparis.com/).
A selection of very nice teas and the shop looks great.
I only buy non-perfumed teas these days and with *Le Conservatoire des Hémisphères* it is very simple to buy great teas: just pick any.

A great classic in Paris is "Marriage Frères".
They have multiple places in Paris and a very large selection of tea.
They do various kinds of teas: non-flavored, very expensive teas, but also flavored, cheap ones.
If you are looking for quality teas, be careful what you are buying.

{% page %}
{% figure %}
![Jugetsudo](/images/2023-02-22_jugetsudo.jpg)
{% figure-caption %}
Jugetsudo
{% /figure-caption %}
{% /figure %}
{% /page %}

Finally, if you are looking for Japanese teas specifically, [Jugetsudo](https://www.openstreetmap.org/node/4307287380) is nice: they have only a few teas, but they are good.

