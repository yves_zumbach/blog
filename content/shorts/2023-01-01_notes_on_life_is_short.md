---
title: Notes on Life is Short
abstract: Three highlights from the article "Life is Short" by Paul Graham.
category: philosophy
---

Here are the tree highlights that I want to take away from the article ["Life is Short"](http://paulgraham.com/vb.html) by Paul Graham:

- "[C]onsciously prioritize bullshit avoidance over other factors like money and prestige."
- "Cultivate a habit of impatience about the things you most want to do. Don't wait before climbing that mountain or writing that book or visiting your mother."
- "Relentlessly prune bullshit, don't wait to do things that matter, and savor the time you have. That's what you do when life is short."

A very nice memento mori.

