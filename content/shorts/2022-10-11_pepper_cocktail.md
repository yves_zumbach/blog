---
title: Pepper Cocktail
abstract: A cocktail flavored with pepper... Is that even a thing?
category: cocktails
---

I wanted to create a cocktail with gin and white tea.
I chose to use jasmine white tea, because it seemed to me that the combination would be interesting and I had some at home.

{% annotation %}
Opportunity is the mother of all inventions...
Or [something](https://en.wikipedia.org/wiki/Necessity_is_the_mother_of_invention) like that.
{% /annotation %}

But I felt something was lacking.
So I decided to try adding pepper to the cocktail (as I had just bought some for me).
But are the flavors of pepper soluble in water?

The molecule that gives spicy heat to pepper, piperine, is soluble in water as per the [wikipedia page](https://en.wikipedia.org/wiki/Piperine).
I tried infusing black pepper in cold water for three hours, but this is not enough.
At all.
No flavors was transferred to the water.

I tried a second time, infusing grounded black pepper for the night.
This gave the pepper taste to water and some heat.
For some subtle cocktail with white tea, I think this is great.
Infusing for more than that means that the other flavors will be crushed.

After infusing the water for 36 hours, the water had a strong and distinctive taste of pepper, with a lot of heat.
This might be interesting for cocktails with stronger flavors.
