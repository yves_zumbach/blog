---
title: Comma-named Commands
abstract: "As the saying goes, there are two hard problems in computer science: off-by-1 errors and naming things. This might solve the second."
category: code
---

Following the advice from [this article](https://rhodesmill.org/brandon/2009/commands-with-comma), I will start all my user-defined commands with a comma from now on.
This prevents name conflicts with commands provided by regular packages.
Also, it makes command completion much more useful: when I type a comma in my terminal, command completion shows all my commands.
