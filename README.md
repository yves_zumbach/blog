# zumbach.dev

Source code of the website [zumbach.dev](https://zumbach.dev).

Built with:

- Next.js (React): Web framework and web server.
- TailwindCSS: CSS library.
- Markdoc: Markdown framework.
- Vercel: Hosting.
- pnpm: Javascript package manager.
- nix: Developer environment.

To start developing, activate the developer environment in the nix flake, then the following commands can be used:

- `dev`: Starts the development server.
- `build`: Compiles the website, i.e. check for errors and builds a production version of the website.

