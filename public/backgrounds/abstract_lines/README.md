# Abstract Line Background Instructions

All svg images should have:

- Lines that are:
  - fully white
  - 1px wide
- Canvas that are fit inside and take as much space as possible inside a 1000px by 1000px square

