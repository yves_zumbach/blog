{
  description = "Blog of Yves Zumbach";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let
        pkgs = import nixpkgs {
          config.allowUnfree = true;
          overlays = [ devshell.overlay ];
          inherit system;
        };
      in {
        devShell = # Used by `nix develop`, value needs to be a derivation
          pkgs.devshell.mkShell {

            commands = [{
              name = "install";
              category = "website";
              help = "Installs the dependencies of the project.";
              command = "${pkgs.writeScript "install" ''
                pnpm install
              ''}";
            }{
              name = "dev";
              category = "website";
              help = "Starts the development server.";
              command = "${pkgs.writeScript "dev" ''
                pnpm dev
              ''}";
            }{
              name = "build";
              category = "website";
              help = "Builds the website.";
              command = "${pkgs.writeScript "dev" ''
                pnpm build
              ''}";
            }{
              name = "b";
              help = "An alias for `build`.";
              category = "website";
              command = "build";
            }{
              category = "website";
              package = pkgs.nodePackages.pnpm;
            }{
              category = "website";
              package = pkgs.nodePackages.svgo;
            }];

            # Specify which packages to add to the shell environment silently
            #packages = with pkgs; [ ];
          };
      }
  );
}
