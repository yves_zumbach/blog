import Portfolio from '../../components/blocs/Portfolio'

export function getStaticProps() {
  return {
    props: {
      title: 'Portfolio',
      description: 'Portfolio of Yves Zumbach.',
    },
  }
}

export default function PortfolioPage({ }) {
  return <Portfolio />
}

