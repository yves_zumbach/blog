import ContainerPage from '../../../components/containers/ContainerPage'

import FullWidth from '../../../components/portfolio/FullWidth'
import ImageLeft from '../../../components/portfolio/ImageLeft'
import ImagePage from '../../../components/portfolio/ImagePage'
import ImageRight from '../../../components/portfolio/ImageRight'
import SideBySide from '../../../components/portfolio/SideBySide'
import Spacer from '../../../components/portfolio/Spacer'
import Title from '../../../components/portfolio/Title'

import SpacerLg from '../../../components/spacers/SpacerLg'
import SpacerMd from '../../../components/spacers/SpacerMd'
import SpacerSm from '../../../components/spacers/SpacerSm'

import { ImageData } from '../../../lib/portfolio'

import IMG_2203 from './IMG_2203.jpg'
const electrical_stairs: ImageData = {
  src: IMG_2203,
  alt: "Electrical stairs.",
  caption: "Rotterdam, The Netherlands | 2023-05-20"
}

import IMG_2209 from './IMG_2209.jpg'
const cube_buildings: ImageData = {
  src: IMG_2209,
  alt: "Square buildings.",
  caption: "Kijk-Kubus, Rotterdam, The Netherlands | 2023-05-20"
}

import yz_002878_v0 from './yz_002878_v0.jpg'
const roof_munich: ImageData = {
  src: yz_002878_v0,
  alt: "Black and white architecture picture",
  caption: "Munich, Germany | 2023-07-14"
}

import yz_002935_v0 from './yz_002935_v0.jpg'
const round_wall_munich: ImageData = {
  src: yz_002935_v0,
  alt: "Black and white architecture building.",
  caption: "Munich, Germany | 2023-07-14"
}

import yz_003167_v0 from './yz_003167_v0.jpg'
const building_windows: ImageData = {
  src: yz_003167_v0,
  alt: "Architectural pattern",
  caption: "Prague, Czech Republic | 2023-07-15"
}

import yz_003571_v0 from './yz_003571_v0.jpg'
const arm_mosque_istanbul: ImageData = {
  src: yz_003571_v0,
  alt: "Inside of a mosque.",
  caption: "Istanbul, Türkiye | 2023-07-20"
}

import yz_003784_v0 from './yz_003784_v0.jpg'
const burj_al_babas_loneliness: ImageData = {
  src: yz_003784_v0,
  alt: "Black and white architecture picture",
  caption: "Burj-al-Babas, Türkiye | 2023-07-22"
}

import yz_003814_v0 from './yz_003814_v0.jpg'
const burj_al_babas_pattern: ImageData = {
  src: yz_003814_v0,
  alt: "Concrete buildings.",
  caption: "Burj-al-Babas, Türkiye | 2023-07-22"
}

import yz_003859_v0 from './yz_003859_v0.jpg'
const church_and_flowers: ImageData = {
  src: yz_003859_v0,
  alt: "Colorful picture of an orthodox church and flowers.",
  caption: "Hagia Sofia | Trabzon, Türkiye | 2023-07-24"
}

import yz_004038_v0 from './yz_004038_v0.jpg'
const corner_of_a_building_tbilisi: ImageData = {
  src: yz_004038_v0,
  alt: "Corner of a building by night",
  caption: "Tbilisi, Georgia | 2023-07-25"
}

import yz_004068_v0 from './yz_004068_v0.jpg'
const stairs_bridge_tbilisi: ImageData = {
  src: yz_004068_v0,
  alt: "Stairs leading to a bridge by night.",
  caption: "Tbilisi, Georgia | 2023-07-25"
}

import yz_004404_v0 from './yz_004404_v0.jpg'
const church_light: ImageData = {
  src: yz_004404_v0,
  alt: "Sunlight through the windows of a church.",
  caption: "Ananuri Church, Georgia | 2023-07-29"
}

import yz_005302_v0 from './yz_005302_v0.jpg'
const abandonned_factory_electrical: ImageData = {
  src: yz_005302_v0,
  alt: "Abandoned factory in black and white.",
  caption: "Chișcădaga, Romania | 2023-08-08"
}

export function getStaticProps({ params }) {
  return {
    props: {
      title: 'Architecture',
      description: 'Some architecture pictures that I realized.',
    }
  }
}

export default function portfolio() {
  return <div className="portfolio">
    <SpacerSm />

    <Title
      text="Architecture"
    />
    <SpacerMd />

    <FullWidth image={roof_munich} />
    <Spacer />

    <SideBySide
      image1={church_and_flowers}
      image2={building_windows}
    />
    <Spacer />

    <ImageLeft image={cube_buildings} />
    <Spacer />

    <ImagePage image={burj_al_babas_pattern} />
    <Spacer />
    
    <ImageRight image={corner_of_a_building_tbilisi} />
    <Spacer />
    
    <FullWidth image={burj_al_babas_loneliness} />
    <Spacer />

    <ImagePage image={round_wall_munich} />
    <Spacer />

    <SideBySide
      image1={stairs_bridge_tbilisi}
      image2={abandonned_factory_electrical}
    />
    <Spacer />

    <ImageLeft image={church_light} />
    <Spacer />

    <ImageRight image={electrical_stairs} />
    <Spacer />

    <FullWidth image={arm_mosque_istanbul} />
    <Spacer />

  </div>
}

