import ContainerPage from '../../../components/containers/ContainerPage'

import FullWidth from '../../../components/portfolio/FullWidth'
import ImageLeft from '../../../components/portfolio/ImageLeft'
import ImagePage from '../../../components/portfolio/ImagePage'
import ImageRight from '../../../components/portfolio/ImageRight'
import SideBySide from '../../../components/portfolio/SideBySide'
import Spacer from '../../../components/portfolio/Spacer'
import Title from '../../../components/portfolio/Title'

import SpacerLg from '../../../components/spacers/SpacerLg'
import SpacerMd from '../../../components/spacers/SpacerMd'
import SpacerSm from '../../../components/spacers/SpacerSm'

import { ImageData } from '../../../lib/portfolio'

import yz_000474 from './yz_000474.jpg'
const cloudy_mountain_anniviers: ImageData = {
  src: yz_000474,
  alt: "Mountain and clouds.",
  caption: "Val d'Anniviers, Switzerland | 2023-06-01",
}

import yz_000497 from './yz_000497.jpg'
const mountain_through_clouds_anniviers: ImageData = {
  src: yz_000497,
  alt: "A mountain hidden in the clouds.",
  caption: "Val d'Anniviers, Switzerland | 2023-06-01",
}

import yz_002282 from './yz_002282.jpg'
const snails_on_plant_on_beach: ImageData = {
  src: yz_002282,
  alt: "Snails on herbs.",
  caption: "Island of Houat, France | 2023-06-24",
}

import yz_002623_v0 from './yz_002623_v0.jpg'
const mountains_and_clouds_georgia: ImageData = {
  src: yz_002623_v0,
  alt: "Mountains and clouds.",
  caption: "Zillertal, Austria | 2023-07-13",
}

import yz_002640_v0 from './yz_002640_v0.jpg'

import yz_002733_v0 from './yz_002733_v0.jpg'
const forest_emerging_from_cloud_zillertal: ImageData = {
  src: yz_002733_v0,
  alt: "Misty forest.",
  caption: "Zillertal, Austria | 2023-07-13",
}

import yz_002791_v0 from './yz_002791_v0.jpg'
const dark_forest_path_zillertal: ImageData = {
  src: yz_002791_v0,
  alt: "Dark forest.",
  caption: "Zillertal, Austria | 2023-07-13",
}

import yz_004353_v0 from './yz_004353_v0.jpg'
const green_plains_on_mountain: ImageData = {
  src: yz_004353_v0,
  alt: "Green mountains.",
  caption: "Stepantsminda, Georgia | 2023-07-29",
}

import yz_004504_v2 from './yz_004504_v2.jpg'
const lit_rocks_prometheus_caves: ImageData = {
  src: yz_004504_v2,
  alt: "Rocks in a cave.",
  caption: "Prometheus caves, Georgia | 2023-07-29",
}

import yz_004795_v0 from './yz_004795_v0.jpg'
const moonrise_georgia: ImageData = {
  src: yz_004795_v0,
  alt: "Moonrise",
  caption: "Zagari Pass, Georgia | 2023-07-30",
}

import yz_004901_v0 from './yz_004901_v0.jpg'
const waterfall_georgia: ImageData = {
  src: yz_004901_v0,
  alt: "Waterfall.",
  caption: "Svaneti Region, Georgia | 2023-08-01",
}

import yz_004926_v0 from './yz_004926_v0.jpg'
const sand_dunes_turkey: ImageData = {
  src: yz_004926_v0,
  alt: "Dunes.",
  caption: "Türkiye | 2023-08-03",
}

import yz_005109_v2 from './yz_005109_v2.jpg'
const pink_forest_romania: ImageData = {
  src: yz_005109_v2,
  alt: "Forest with pink trees.",
  caption: "Sinaia, Romania | 2023-08-05",
}

export function getStaticProps({ params }) {
  return {
    props: {
      title: 'Nature',
      description: 'Some nature pictures that I realized.',
    }
  }
}

export default function portfolio() {
  return <div className="portfolio">
    <SpacerSm />

    <Title
      text="Nature"
    />
    <SpacerMd />

    <FullWidth image={mountain_through_clouds_anniviers} />
    <Spacer />

    <ImagePage image={green_plains_on_mountain} />
    <Spacer />

    <SideBySide
      image1={cloudy_mountain_anniviers}
      image2={snails_on_plant_on_beach}
    />
    <Spacer />

    <FullWidth image={sand_dunes_turkey} />
    <Spacer />

    <SideBySide
      image1={forest_emerging_from_cloud_zillertal}
      image2={dark_forest_path_zillertal}
    />
    <Spacer />

    <ImagePage image={lit_rocks_prometheus_caves} />
    <Spacer />

    <FullWidth image={moonrise_georgia} />
    <Spacer />

    <ImageLeft image={waterfall_georgia} />
    <Spacer />

    <ImagePage image={mountains_and_clouds_georgia} />
    <Spacer />

    <FullWidth image={pink_forest_romania} />
    <Spacer />

  </div>
}

