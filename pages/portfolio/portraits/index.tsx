import ContainerPage from '../../../components/containers/ContainerPage'

import FullWidth from '../../../components/portfolio/FullWidth'
import ImageLeft from '../../../components/portfolio/ImageLeft'
import ImagePage from '../../../components/portfolio/ImagePage'
import ImageRight from '../../../components/portfolio/ImageRight'
import SideBySide from '../../../components/portfolio/SideBySide'
import Spacer from '../../../components/portfolio/Spacer'
import Title from '../../../components/portfolio/Title'

import SpacerLg from '../../../components/spacers/SpacerLg'
import SpacerMd from '../../../components/spacers/SpacerMd'
import SpacerSm from '../../../components/spacers/SpacerSm'

import { ImageData } from '../../../lib/portfolio'

import IMG_1930 from './IMG_1930.jpg'
const ac_renaissance_profile: ImageData = {
  src: IMG_1930,
  alt: "Portrait of a young woman.",
  caption: "Anne-Claire | Paris, France | 2023-04-29",
}

import IMG_1973 from './IMG_1973.jpg'
const ac_renaissance_3_4: ImageData = {
  src: IMG_1973,
  alt: "Portrait of a young woman.",
  caption: "Anne-Claire | Paris, France | 2023-04-29",
}

import yz_000026 from './yz_000026.jpg'
const tiken: ImageData = {
  src: yz_000026,
  alt: "Singer holding a mic.",
  caption: "Singer | Geneva, Switzerland | 2023-05-24",
}

import yz_001268 from './yz_001268.jpg'
const firebreather: ImageData = {
  src: yz_001268,
  alt: "A firebreather with a big flame.",
  caption: "Firebreather | Médiévalles d'Andilly, France | 2023-06-10",
}

import yz_001868_v1 from './yz_001868_v1.jpg'
const ac_bw_smiling: ImageData = {
  src: yz_001868_v1,
  alt: "Black and white portrait of a young woman.",
  caption: "Anne-Claire | Saint-Philibert, France | 2023-06-20",
}

import yz_001873_v1 from './yz_001873_v1.jpg'
const ac_closeup_eye: ImageData = {
  src: yz_001873_v1,
  alt: "Black and white portrait of a young woman.",
  caption: "Anne-Claire | Saint-Philibert, France | 2023-06-20",
}

import yz_003953_v0 from './yz_003953_v0.jpg'
const turkish_music_player: ImageData = {
  src: yz_003953_v0,
  caption: "Music Player | Sumela Monastery, Türkiye | 2023-07-24",
  alt: "A music player.",
}

import yz_003261_v1 from './yz_003261_v1.jpg'
const anne_red_blue: ImageData = {
  src: yz_003261_v1,
  caption: "Anne | Prague, Czech Republic | 2023-07-15",
  alt: "Half portrait with mostly blue and red colors.",
}

import yz_004705_v0 from './yz_004705_v0.jpg'
const anne_under_giant_plants: ImageData = {
  src: yz_004705_v0,
  caption: "Anne | Svaneti, Georgia | 2023-07-30",
  alt: "Melancholy under oversized plants.",
}

export function getStaticProps({ params }) {
  return {
    props: {
      title: 'Portraits',
      description: 'Some portrait pictures that I realized.',
    }
  }
}

export default function portfolio() {
  return <div className="portfolio">
    <SpacerSm />

    <Title
      text="Portraits"
    />
    <SpacerMd />

    <FullWidth image={ac_bw_smiling} />
    <Spacer />

    <ImageLeft image={anne_red_blue} />
    <Spacer />

    <SideBySide
      image1={firebreather}
      image2={turkish_music_player} />
    <Spacer />

    <ImagePage image={ac_closeup_eye} />
    <Spacer />

    <ImageLeft image={anne_under_giant_plants} />
    <Spacer />

    <ImageRight image={tiken} />
    <Spacer />

    <SideBySide
      image1={ac_renaissance_profile}
      image2={ac_renaissance_3_4} />
    <Spacer />

  </div>
}

// Anne-Claire, no consent yet.
//import IMG_1998 from './IMG_1998.jpg'
//import yz_002031_v1 from './yz_002031_v1.jpg'
//import yz_002035_v1 from './yz_002035_v1.jpg'

/*
<SideBySide
  src1={IMG_1930}
  caption1="Anne-Claire | Paris, FR | 2023-04-29"
  alt1="Black and white portrait of a young woman."
  src2={IMG_1998}
  caption2="Anne-Claire | Paris, FR | 2023-04-29"
  alt2="Black and white portrait of a young woman."
/>
<Spacer />
*/

/*
<ImageRight
  src={yz_002031_v1}
  caption="Anne-Claire | Brittany, FR | 2023-07-07"
  alt="Black and white portrait of a young woman."
/>
<Spacer />
*/

