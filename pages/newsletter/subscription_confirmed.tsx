import Head from 'next/head'

import Button from '../../components/Button'
import MinHeightScreenBackgroundSvg from '../../components/backgrounds/MinHeightScreenBackgroundSvg'
import SpacerSm from '../../components/spacers/SpacerSm'

export function getStaticProps({ params }) {
  return {
    props: {
      title: 'Welcome aboard!',
      description: 'Congratulation, you are now enrolled to the zumbach.dev newsletter.',
    }
  }
}

export default function Confirmed() {
  return <>
    <MinHeightScreenBackgroundSvg
      imagePath="/backgrounds/abstract_lines/3.min.svg"
    >
      <h1 className="h1">
        Welcome aboard!
      </h1>
      <p>
        Thanks for subscribing to my newsletter.
      </p>

      <SpacerSm />
      <Button text="back to home" href="/" />
    </MinHeightScreenBackgroundSvg>
  </>
}


