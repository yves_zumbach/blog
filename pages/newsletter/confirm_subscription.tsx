import Head from 'next/head'

import Button from '../../components/Button'
import MinHeightScreenBackgroundSvg from '../../components/backgrounds/MinHeightScreenBackgroundSvg'
import SpacerSm from '../../components/spacers/SpacerSm'

export function getStaticProps({ params }) {
  return {
    props: {
      title: 'One last thing...',
      description: 'You must confirm your subscription to the newsletter by clicking the link I sent you by email.',
    }
  }
}

export default function NeedToConfirm() {
  return <>
    <MinHeightScreenBackgroundSvg
      imagePath="/backgrounds/abstract_lines/3.min.svg"
    >
      <h1 className="h1">
        One last thing...
      </h1>
      <p>
        To ensure it is really you, I need you to confirm your enrollment by clicking the link in the email I just sent you.
      </p>

      <SpacerSm />
      <Button text="back to home" href="/" />
    </MinHeightScreenBackgroundSvg>
  </>
}

