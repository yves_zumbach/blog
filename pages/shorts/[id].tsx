import React from "react";
import Markdoc from '@markdoc/markdoc'

import { getShortsPath, getShortFromId } from '../../lib/shorts'
import { runtimeComponents } from '../../lib/markdoc'

import BlocNewsletter from '../../components/blocs/BlocNewsletter'
import ContainerProse from '../../components/containers/ContainerProse'
import SpacerLg from '../../components/spacers/SpacerLg'
import SpacerSm from '../../components/spacers/SpacerSm'

export function getStaticPaths() {
  const paths = getShortsPath()
  return {
    paths,
    fallback: false,
  }
}

export function getStaticProps({ params }) {
  const short = getShortFromId(params.id)
  return {
    props: {
      title: short.title,
      description: short.abstract,
      short,
    },
  }
}

export default function Short({ short }) {
  const parsedContent = Markdoc.renderers.react(JSON.parse(short.content), React, runtimeComponents)
  return <>
    <article className="short">
      <SpacerLg />

      <ContainerProse>
        <div className="font-sans font-semibold text-lg lowercase small-caps text-gray-200">
          short
        </div>
        <h1 className="font-sans font-bold text-4xl lowercase small-caps text-gray-300">
          {short.title}
        </h1>
        <SpacerSm />
      </ContainerProse>

      <div className="article">
        {parsedContent}
      </div>
    </article>

    <div className="clear-both">
      <BlocNewsletter />
    </div>
  </>
}


