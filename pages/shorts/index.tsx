import { getShorts } from '../../lib/shorts'

import Shorts from '../../components/blocs/Shorts'

export function getStaticProps() {
  const shorts = getShorts()
  return {
    props: {
      title: 'Shorts',
      description: 'The list of all shorts on zumbach.dev',
      shorts,
    },
  }
}

export default function ShortsPage({ shorts }) {
  return <>
    <Shorts shorts={shorts} />
  </>
}

