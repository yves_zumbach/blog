import { getCategories } from '../lib/categories'
import { getPosts } from '../lib/posts'
import { getShorts } from '../lib/shorts'

import BouncingDownArrow from '../components/BouncingDownArrow'
import MinHeightScreenBackgroundSvg from '../components/backgrounds/MinHeightScreenBackgroundSvg'
import BlocNewsletter from '../components/blocs/BlocNewsletter'
import Portfolio from '../components/blocs/Portfolio'
import Posts from '../components/blocs/Posts'
import Shorts from '../components/blocs/Shorts'
import SpacerSm from '../components/spacers/SpacerSm'

export function getStaticProps() {
  const posts = getPosts()
  const shorts = getShorts()
  return {
    props: {
      title: 'zumbach.dev',
      noTitleSuffix: true,
      description: 'A personal blog for my thoughts and ranting about computer science, ecology, philosophy, and any other topic that I find interesant.',
      posts,
      shorts,
    },
  }
}

export default function Home({ posts, shorts }) {
  return <>
    <MinHeightScreenBackgroundSvg
      backgroundClasses="bg-gray-950 border-b border-gray-700"
      imagePath="/backgrounds/abstract_lines/3.min.svg"
    >
      <div className="font-sans font-semibold text-4xl lowercase small-caps">
        zumbach.dev
      </div>
      <div className="mt-4">
        My thoughts and ranting about computer science, ecology, and tea.
      </div>
      <div className="mt-24 text-center italic text-gray-300">
        <div className="inline-block relative">
          <img className="absolute -translate-y-3 -translate-x-8 w-8 opacity-50" src="/typo/apostrophe.min.svg" alt="apostrophe" />
          Moins mais mieux.
        </div>
      </div>

      <BouncingDownArrow href="#posts" />
    </MinHeightScreenBackgroundSvg>

    <div id="posts">
      <Posts
        posts={posts}
        mostRecentOnly={true}
      />
    </div>
    
    <Shorts
      shorts={shorts}
      mostRecentOnly={true}
    />

    <Portfolio />

    <BlocNewsletter />
  </>
}

