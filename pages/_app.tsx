/* 
 * Used to define a layout for all pages.
 */

import Head from 'next/head'
import Script from 'next/script'
import type { AppProps } from 'next/app'
import localFont from '@next/font/local'
import type { MarkdocNextJsPageProps } from '@markdoc/next.js'
import { Analytics } from '@vercel/analytics/react';

// Syntax highlighting for code.
import 'prismjs'
import 'prismjs/components/prism-bash.min'
import 'prismjs/themes/prism-okaidia.min.css'

import { assert } from '../lib/utils'

import Footer from '../components/Footer'

// Local CSS
import '../style/globals.css'

// Fonts
const fontSourceSans = localFont({
  src:[
    {
      path: './fonts/source_sans_3/source_sans_3_variable_roman.otf.woff2',
    },
    // {
    //   path: './fonts/source_sans_3/source_sans_3_variable_italic.otf.woff2',
    //   style: 'italic',
    // },
  ],
  variable: '--font-source-sans',
})
const fontSourceSerif = localFont({
  src:[
    {
      path: './fonts/source_serif_4/source_serif_4_variable_roman.otf.woff2',
    }, {
      path: './fonts/source_serif_4/source_serif_4_variable_italic.otf.woff2',
      style: 'italic',
    },
  ],
  variable: '--font-source-serif',
})
const fontSourceCode = localFont({
  src:[
    {
      path: './fonts/source_code_pro/source_code_pro_variable_roman.otf.woff2',
    }, {
      path: './fonts/source_code_pro/source_code_pro_variable_italic.otf.woff2',
      style: 'italic',
    },
  ],
  variable: '--font-source-code',
  preload: false,
})

type Props = MarkdocNextJsPageProps & {
  // The title to set to the page.
  title: string,
  // A description of the page content, used to set as page metadata.
  description: string,
  // Prevents the "zumbach.dev" suffic from being added to the provided page title.
  noTitleSuffix?: boolean,
}

export default function MyApp({ Component, pageProps }: AppProps<Props>) {
  let { title } = pageProps
  const { description, noTitleSuffix } = pageProps

  assert(title !== '', "A page title must be defined, but none was given.")
  assert(description !== '', "A page description must be defined, but none was given.")

  if (!noTitleSuffix) {
    title = title + ' | zumbach.dev'
  }

  return <>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="referrer" content="strict-origin" />
      {/*
      <link rel="shortcut icon" href="/favicon.ico" />
      <link rel="icon" href="/favicon.ico" />
      */}

      <title>{title}</title>
      <meta name="title" content={title} />
      <meta name="description" content={description} />
    </Head>
    {/* Sends an email in case my website gets cloned. See https://docs.canarytokens.org/guide/cloned-web-token.html#what-is-a-cloned-website-token. */}
    <Script id="canary" strategy="lazyOnload">
      {`
        if (!document.domain.endsWith("zumbach.dev") && !document.domain.endsWith("vercel.app") && document.domain !== "localhost" && document.domain !== "127.0.0.1") {
          var l = location.href;
          var r = document.referrer;
          var m = new Image();
          m.src = "https://canarytokens.com/"+
                  "49i4isbnx5q021mhyxdykfka5.jpg?l="+
                  encodeURI(l) + "&amp;r=" + encodeURI(r);
        }
      `}
    </Script>
    <Analytics />

    {/* Load CSS variable of my fonts. */}
    <div className={`${fontSourceSans.variable} ${fontSourceSerif.variable} ${fontSourceCode.variable} font-serif oldstyle-nums scroll-smooth min-h-screen flex flex-col justify-between`}>
      <main className="grow">
        <Component {...pageProps} />
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  </>
}

