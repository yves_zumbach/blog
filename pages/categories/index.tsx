import { getCategories } from '../../lib/categories'

import CategoriesAll from '../../components/blocs/CategoriesAll'
import MinHeightScreenBackgroundSvg from '../../components/backgrounds/MinHeightScreenBackgroundSvg'
import SpacerSm from '../../components/spacers/SpacerSm'

export function getStaticProps() {
  const categories = getCategories()
  return {
    props: {
      title: 'Categories',
      description: 'The list of all categories of content on zumbach.dev.',
      categories,
    },
  }
}

export default function Categories({ categories }) {
  return (
    <>
      <MinHeightScreenBackgroundSvg
        imagePath="/backgrounds/abstract_lines/2.min.svg"
        imageClasses="ml-auto mr-8"
      >
        <h1 className="h1">
          Categories
        </h1>
        <SpacerSm />
        <CategoriesAll categories={categories} />
      </MinHeightScreenBackgroundSvg>
    </>
  )
}

