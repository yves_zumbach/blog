import { Category, getCategoryFromId, getCategoriesPath } from '../../lib/categories'
import { Post, getPostsWithCategoryId } from '../../lib/posts'
import { Short, getShortsWithCategoryId } from '../../lib/shorts'

import BouncingDownArrow from '../../components/BouncingDownArrow'
import MinHeightScreenBackground from '../../components/backgrounds/MinHeightScreenBackground'
import Posts from '../../components/blocs/Posts'
import Shorts from '../../components/blocs/Shorts'
import SpacerSm from '../../components/spacers/SpacerSm'

export function getStaticPaths() {
  const paths = getCategoriesPath()
  return {
    paths,
    fallback: false,
  }
}

export function getStaticProps({ params }) {
  const category = getCategoryFromId(params.id)
  const posts = getPostsWithCategoryId(category.id)
  const shorts = getShortsWithCategoryId(category.id)
  return {
    props: {
      title: `Category ${category.name}`,
      description: category.description,
      category,
      posts,
      shorts,
    },
  }
}

export default function({ category, posts, shorts }: { category: Category, posts: Post[], shorts: Short[]}) {
  return (
    <>
      <MinHeightScreenBackground
        imagePath={category.image.path}
        imageAlt={category.image.alt}
      >
        <h1 className="h1">
          Category {category.name}
        </h1>
        {/*
        <div className="mt-4">
          {category.description}
        </div>
        */}
        <BouncingDownArrow href="#content" />
      </MinHeightScreenBackground>

      <div id="content">
        {posts.length == 0 ? null : <Posts posts={posts} />}

        {shorts.length == 0 ? null : <Shorts shorts={shorts} />}
      </div>
    </>
  )
}

