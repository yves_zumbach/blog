import { getPosts } from '../../lib/posts'

import Posts from '../../components/blocs/Posts'

export function getStaticProps() {
  const posts = getPosts()
  return {
    props: {
      title: 'Posts',
      description: 'The list of all posts on zumbach.dev',
      posts,
    },
  }
}

export default function PostsPage({ posts }) {
  return <>
    <Posts posts={posts} />
  </>
}
