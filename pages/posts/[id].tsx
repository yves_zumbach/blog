import React from "react";
import Markdoc from '@markdoc/markdoc'

import { getPostsPath, getPostFromId } from '../../lib/posts'
import { runtimeComponents } from '../../lib/markdoc'
import { dateToYearMonthDay } from '../../lib/utils'

import BouncingDownArrow from '../../components/BouncingDownArrow'
import BlocNewsletter from '../../components/blocs/BlocNewsletter'
import MinHeightScreenBackground from '../../components/backgrounds/MinHeightScreenBackground'
import SpacerMdCollapsible from '../../components/spacers/SpacerMdCollapsible'

export function getStaticPaths() {
  const paths = getPostsPath()
  return {
    paths,
    fallback: false,
  }
}

export function getStaticProps({ params }) {
  const post = getPostFromId(params.id)
  return {
    props: {
      title: post.title,
      description: post.abstract,
      noBackground: true,
      post,
    },
  }
}

export default function Post({ post }) {
  const renderedContent = Markdoc.renderers.react(JSON.parse(post.content), React, runtimeComponents)
  return <>
    <article className="post" style={{contain: 'paint'}}>
      <MinHeightScreenBackground
        imagePath={post.coverImage.path}
        imageAlt={post.coverImage.alt}
        imageClasses="grayscale-std"
      >
        <div className="font-sans font-bold text-3xl lowercase small-caps text-gray-100 opacity-80">
          post
        </div>
        <h1 className="font-sans font-bold text-5xl md:text-6xl lowercase small-caps text-gray-100 opacity-90">
          {post.title}
        </h1>
        <div className="mt-4 flex justify-center">
          <div className="text-lg text-gray-100 opacity-90">
            {dateToYearMonthDay(post.date)}
          </div>
        </div>

        <BouncingDownArrow href="#contentStart" />
      </MinHeightScreenBackground>

      <SpacerMdCollapsible />

      <div id="contentStart" className="article">
        {renderedContent}
      </div>
    </article>

    <div className="clear-both">
      <BlocNewsletter />
    </div>
  </>
}

