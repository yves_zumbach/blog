import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import Markdoc from '@markdoc/markdoc'

import { assert, imagesDirectory } from './utils'
import { Category, CategoryId, getCategoryIdToCategory, getCategoriesId } from './categories'

import { config } from './markdoc'

const shortsDirectory = path.join(process.cwd(), 'content/shorts')

export type ShortId = string

export interface Short {
  id: ShortId
  title: string
  url: string,
  filepath: string,
  date: string
  metaTitle: string
  abstract: string
  category: Category
  content: string // JSON serialized Markdoc AST, NextJS does not support JS objects in props.
}

type UserDefinedShortMetadata = Omit<Short, 'content' | 'date' | 'filepath' | 'id' | 'metaTitle' | 'url' >

export type ShortMetadata = Omit<Short, 'content'>

/**
 * Checks that the `frontmatter` of the short written at `filename` contains all required metadata, then returns a `UserDefinedShortMetadata` object.
 */
function frontmatterToUserDefinedShortMetadata(frontmatter: any, filename: string): UserDefinedShortMetadata {
  assert(frontmatter.title !== undefined, `Missing title metadata in short ${filename}.`)
  assert(frontmatter.abstract !== undefined, `Missing abstract metadata in short ${filename}.`)
  assert(frontmatter.category !== undefined, `Missing category metadata in short ${filename}.`)
  const categoryIdToCategory = getCategoryIdToCategory()
  assert(frontmatter.category in categoryIdToCategory, `Short ${filename} has category "${frontmatter.category}" which is not allowed. Please create a new category or use one of: ${getCategoriesId()}.`)
  const category = categoryIdToCategory[frontmatter.category]

  return {
    title: frontmatter.title,
    abstract: frontmatter.abstract,
    category,
  }
}

let _shortsMetadata: ShortMetadata[] = null

/**
 * Returns the metadata of all the shorts in the `/shorts` folder.
 * The shorts will be sorted by descending date.
 */
export function getShortsMetadata() {
  if (!_shortsMetadata) {
    // Get file names under /content/shorts
    const filenames = fs.readdirSync(shortsDirectory)
    const shortsMetadata = filenames.map((filename) => {
      // First, check that the file name is valid.
      const filenameRegex = /^\d{4}-\d{2}-\d{2}_[\w_]+\.md(oc)?$/
      if (!filenameRegex.test(filename)) {
        throw `Short with path ${filename} has an invalid file name. Please conform to ${filenameRegex}`
      }

      // Remove ".md" from file name to get id
      const date = filename.substring(0, 10)
      const id = filename.replace(/\.md(oc)?$/, '').substring(11)

      // Read markdown file as string
      const fullPath = path.join(shortsDirectory, filename)
      const fileContent = fs.readFileSync(fullPath, 'utf8')

      // Use gray-matter to parse the short's metadata section
      const frontmatter = matter(fileContent).data
      // Ensure all required metadata are present and convert to internal representation.
      const userDefinedShortMetadata = frontmatterToUserDefinedShortMetadata(frontmatter, fullPath)

      let metaTitle = 'metaTitle' in frontmatter ? frontmatter.metaTitle : frontmatter.title

      const shortMetadata: ShortMetadata = {
        id,
        url: `/shorts/${id}`,
        ...userDefinedShortMetadata,
        metaTitle,
        // Using the ISO 8601 representation seems appropriate here.
        date: new Date(date).toISOString(),
        filepath: fullPath,
      }

      return shortMetadata
    })

    const shortsId = shortsMetadata.map(({ id }) => { return id })
    const uniqueShortsId = new Set(shortsId)
    if (shortsId.length !== uniqueShortsId.size) {
      throw `Some shorts have identical IDs, where IDs are computed as the filename without the date and the extension. This is invalid. Please ensure that all shorts' ID are unique. Computed IDs: ${shortsId}.`
    }

    // Sort shorts by date
    _shortsMetadata = shortsMetadata.sort(({ date: a }, { date: b }) => {
      if (a < b) {
        return 1
      } else if (a > b) {
        return -1
      } else {
        return 0
      }
    })
  }
  return _shortsMetadata
}

/** Stores a mapping from short ID to the short metadata. */
let _shortIdToShortMetadata: Map<ShortId, ShortMetadata> = null

export function getShortIdToShortMetadata() {
  if (!_shortIdToShortMetadata) {
    const shortsMetadata = getShortsMetadata()
    _shortIdToShortMetadata = new Map()
    for (const shortMetadata of shortsMetadata) {
      _shortIdToShortMetadata[shortMetadata.id] = shortMetadata
    }
  }
  return _shortIdToShortMetadata
}

export function getShortMetadataFromId(id: string) {
  const shortIdToShortMetadata = getShortIdToShortMetadata()
  assert(id in shortIdToShortMetadata, `There is no short with ID ${id}. Please use a valid short ID.`)
  return shortIdToShortMetadata[id]
}

let _shorts: Short[] = null

/**
 * Returns the shorts in the `content/shorts/` folder, sorted by descending date.
 */
export function getShorts() {
  // Lazy function: we only read the shorts once per build.
  if (!_shorts) {
    // Get file names under /content/shorts
    const shorts = getShortsMetadata().map((shortMetadata) => {
      // Read markdown file as string
      const fileContent = fs.readFileSync(shortMetadata.filepath, 'utf8')

      const ast = Markdoc.parse(fileContent)
      const errors = Markdoc.validate(ast, config);
      assert(errors.length == 0, `Short ${shortMetadata.id} contains invalid Markdoc syntax. Errors were: ${JSON.stringify(errors)}`)
      const content = Markdoc.transform(ast, config)

      const short: Short = {
        ...shortMetadata,
        content: JSON.stringify(content), // We need to stringify the Markdoc AST because NextJS does not support objects in props.
      }

      return short
    })

    // Store the shorts
    _shorts = shorts
  }
  return _shorts
}

/** Stores a mapping from short ID to the short itself for lazy functions. */
let _shortIdToShort: Map<ShortId, Short> = null

export function getShortIdToShort() {
  if (!_shortIdToShort) {
    const shorts = getShorts()
    _shortIdToShort = new Map()
    for (const short of shorts) {
      _shortIdToShort[short.id] = short
    }
  }
  return _shortIdToShort
}

export function getShortFromId(id: string) {
  const shortIdtoShort = getShortIdToShort()
  assert(id in shortIdtoShort, `There is no short with ID ${id}. Please use a valid short ID.`)
  return shortIdtoShort[id]
}

export function getShortsPath() {
  const shorts = getShorts()
  return shorts.map(({ id }) => {
    return { params: { id } }
  })
}

export function getShortsWithCategoryId(categoryId: CategoryId): Short[] {
  return getShorts().filter(({ category }) => category.id === categoryId)
}

