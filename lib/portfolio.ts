export interface ImageData {
  /** Image data statically imported. */
  src: any // StaticImageData
  /** A description of the image for blind people. */
  alt: string
  /** Cation shown around the image. */
  caption: string
}
