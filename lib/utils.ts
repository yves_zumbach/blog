import path from 'path'

export const imagesDirectory = path.join(process.cwd(), 'public/')

export function assert(condition: boolean, errorMessage: string) {
  if (!condition) throw errorMessage
}

export function zip<S, T>(x: Array<S>, y: Array<T>) {
  return Array.from(Array(Math.min(x.length, y.length)), (_, i) => [x[i], y[i]])
}

const formatToTwoDigits = new Intl.NumberFormat('en-US', { minimumIntegerDigits: 2 })

export function dateToYearMonthDay(date: string): string {
  const d = new Date(date)
  return `${d.getFullYear()}-${formatToTwoDigits.format(d.getMonth() + 1)}-${formatToTwoDigits.format(d.getDate())}`
}

