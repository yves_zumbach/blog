import fs from 'fs'
import path from 'path'
import yaml from 'yaml'
import sizeOf from 'image-size'

import { Image } from './images'
import { assert, imagesDirectory } from './utils'

export type CategoryId = string

export interface Category {
  /** The unique identifier of this category. Used to reference the category. */
  id: CategoryId
  /** The human readable name of the category. */
  name: string
  /** The background image of the category. */
  image: Image
  /** The path to an SVG icon for the category. */
  iconPath: string

  description: string
}

const categoriesDirectory = path.join(process.cwd(), 'content/categories')

function createCategoryFromMetadata(filename: string, metadata: any): Category {

  const id = filename.replace(/\.yaml$/, '')
  
  assert(metadata !== undefined && metadata !== null, `File ${filename} is empty while it should contain a full category specification. Please fill the file with the required category metadata or delete the file.`)

  assert(metadata.name !== undefined, `Category defined in file ${filename} is missing name metadata field.`)
  const name = metadata.name

  assert(metadata.imagePath !== undefined, `Category in file ${filename} is missing imagePath metadata field.`)
  const imagePath = path.join(imagesDirectory, metadata.imagePath)
  const { height, width } = sizeOf(imagePath)

  assert(metadata.iconPath !== undefined, `Category in file ${filename} is missing iconPath metadata field.`)
  assert(metadata.description !== undefined, `Category in file ${filename} is missing description metadata field.`)
  
  return {
    id,
    name,
    image: {
      path: metadata.imagePath,
      width,
      height,
      alt: '',
    },
    iconPath: metadata.iconPath,
    description: metadata.description,
  }
}

let _categories: Category[] = null

export function getCategories(): Category[] {
  if (!_categories) {
    // Get file names under /content/categories
    const filenames = fs.readdirSync(categoriesDirectory)
    const categories = filenames.map((filename) => {
      const fullPath = path.join(categoriesDirectory, filename)
      const fileContent = fs.readFileSync(fullPath, 'utf8')
      return createCategoryFromMetadata(filename, yaml.parse(fileContent))
    })

    const categoriesId = categories.map(({ id }) => { return id })
    const uniqueCategoriesId = new Set(categoriesId)
    if (categoriesId.length !== uniqueCategoriesId.size) {
      throw `Some categories have identical IDs, where IDs are computed as the name of the file in which the category is defined without the extension. This is invalid. Please ensure that each category has a unique ID. Computed IDs: ${categoriesId}.`
    }

    // Sort categories alphabetically
    _categories = categories.sort(({ name: nameA }, { name: nameB }) => {
      return nameA.localeCompare(nameB)
    })
  }

  return _categories
}

let _categoryIdToCategory: Map<CategoryId, Category> = null

export function getCategoryIdToCategory(): Map<CategoryId, Category> {
  if (!_categoryIdToCategory) {
    const categories: Category[] = getCategories()
    _categoryIdToCategory = new Map()
    for (const category of categories) {
      _categoryIdToCategory[category.id] = category
    }
  }
  return _categoryIdToCategory
}

export function getCategoryFromId(id: CategoryId): Category {
  return getCategoryIdToCategory()[id]
}

export function getCategoriesId(): CategoryId[] {
  return Array.from(getCategories().map(({ id }) => id))
}

export function getCategoriesPath() {
  const categories = getCategories()
  return categories.map(({ id }) => {
    return { params: { id } }
  })
}

