import { Tag } from '@markdoc/markdoc'

export default {
  description: 'A heading taking a large portion of the screen and associated with an image.',
  children: [],
  attributes: {
    title: {
      required: true,
      type: String,
    },
    imagePath: {
      required: true,
      type: String,
    },
    imageAlt: {
      required: true,
      type: String,
    },
  },
  transform(node, config) {
    const attributes = node.transformAttributes(config)
    const children = node.transformChildren(config)
    const id = attributes.title
      .replace(/[?]/g, '')
      .replace(/\s+/g, '-')
      .toLowerCase()

    return new Tag(
      'Chapter',
      {
        ...attributes,
        id
      },
      children
    )
  },
}

