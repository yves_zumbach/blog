import { Tag } from '@markdoc/markdoc'

export default {
  render: 'YoutubeVideo',
  description: 'A Youtube video.',
  children: [ ],
  attributes: {
    src: {
      required: true,
      type: String,
    },
  },
}
