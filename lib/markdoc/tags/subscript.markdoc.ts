export default {
  render: 'sub',
  description: 'Typesets the content as subscript.',
  children: [ 'text' ],
}
