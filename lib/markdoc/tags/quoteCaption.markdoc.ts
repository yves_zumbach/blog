import { Tag } from '@markdoc/markdoc'

export default {
  render: 'QuoteCaption',
  description: 'A caption of a quote.',
}
