export default {
  render: 'Quote',
  description: 'A quote.',
  attributes: {
    caption: {
      required: false,
      type: String,
    },
  },
}
