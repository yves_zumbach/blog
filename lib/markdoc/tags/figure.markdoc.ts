import { Tag } from '@markdoc/markdoc'

export default {
  render: 'Figure',
  description: 'A figure.',
}
