import { Tag } from '@markdoc/markdoc'

export default {
  render: 'FigureCaption',
  description: 'A caption of a figure.',
}
