import { Tag } from '@markdoc/markdoc'
import { getPostMetadataFromId } from '../../posts'
import { WIDTHS_PROSE, WIDTHS_ANNOTATION, WIDTHS_PAGE } from '../../images'

export default {
  description: 'The card of an article.',
  children: [ ],
  attributes: {
    id: {
      required: true,
      type: String,
    },
    location: {
      required: false,
      type: String,
      matches: ['prose', 'annotation', 'page'],
      default: 'prose'
    },
  },
  transform(node, config) {
    const attributes = node.transformAttributes(config)
    const children = node.transformChildren(config)

    const post = getPostMetadataFromId(attributes.id)
    let containerWidths = WIDTHS_PROSE
    if (attributes.location === 'annotation') {
      containerWidths = WIDTHS_ANNOTATION
    } else if (attributes.location === 'page') {
      containerWidths = WIDTHS_PAGE
    }

    return new Tag(
      'CardPostHorizontal',
      {
        ...attributes,
        post,
        containerWidths,
      },
      children
    )
  },
}
