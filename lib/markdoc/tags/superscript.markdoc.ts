export default {
  render: 'sup',
  description: 'Typesets the content as superscript.',
  children: [ 'text' ],
}
