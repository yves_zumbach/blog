import Markdoc, { Schema, Tag } from '@markdoc/markdoc'

import heading from './nodes/heading.markdoc'
import image from './nodes/image.markdoc'

import cardPostHorizontal from './tags/cardPostHorizontal.markdoc'
import cardPostVertical from './tags/cardPostVertical.markdoc'
import chapter from './tags/chapter.markdoc'
import figure from './tags/figure.markdoc'
import figureCaption from './tags/figureCaption.markdoc'
import quote from './tags/quote.markdoc'
import quoteCaption from './tags/quoteCaption.markdoc'
import sub from './tags/subscript.markdoc'
import sup from './tags/superscript.markdoc'
import youtubeVideo from './tags/youtubeVideo.markdoc'

// Example for how nodes are defined: https://github.com/markdoc/markdoc/blob/main/src/schema.ts

const proseConfig = {
  nodes: {
    heading,
    image,
  },
  tags: {
    'card-post-horizontal': cardPostHorizontal,
    'card-post-vertical': cardPostVertical,
    figure,
    'figure-caption': figureCaption,
    quote,
    'quote-caption': quoteCaption,
    sub,
    sup,
    'youtube-video': youtubeVideo,
  }
}

const annotation = {
  children: [
    'blockquote',
    'comment',
    'fence',
    'heading',
    // 'hr',
    'image',
    'list',
    'paragraph',
    'table',
    'tag',
  ],
  description: 'Prints annotations in the margin',
  transform(node, config) {
    return new Tag(
      'ContainerAnnotation',
      node.transformAttributes(config),
      [ Markdoc.transform(node, proseConfig) ]
    )
  },
}

const page = {
  children: [
    'blockquote',
    'comment',
    'fence',
    'heading',
    // 'hr',
    'image',
    'list',
    'paragraph',
    'table',
    'tag',
  ],
  description: 'Prints content using the width of the page.',
  transform(node, config) {
    return new Tag(
      'ContainerPage',
      node.transformAttributes(config),
      [ Markdoc.transform(node, proseConfig) ]
    )
  },
}

const fullWidth = {
  children: [
    'blockquote',
    'comment',
    'fence',
    // 'heading',
    // 'hr',
    'image',
    'list',
    'paragraph',
    'table',
    'tag',
  ],
  description: 'Prints content using the full width of the screen.',
  transform(node, config) {
    return new Tag(
      'ContainerFullWidth',
      node.transformAttributes(config),
      [ Markdoc.transform(node, proseConfig) ]
    )
  },
}

// Tags that define their own container.
const containerConfig = {
  tags: {
    annotation,
    'full-width': fullWidth,
    page,
    chapter,
  },
}
const containerTagsName = new Set(Object.keys(containerConfig.tags))

// The document node contains all other.
// We use it to wrap prose elements into a prose container.
// All content that should be inserted into a different container should have a tag that adds this container.
const document = {
  children: [ 
    'blockquote',
    'comment',
    'fence',
    'heading',
    'hr',
    'image',
    'list',
    'paragraph',
    'table',
    'tag',
  ],
  attributes: {
    frontmatter: { render: false },
  },
  description: 'Prints regular markdown nodes inside a prose container using the prose configuration and uses the tag configuration for all other nodes, i.e. tags.',
  transform(node, config) {
    let transformed = []
    for (const child of node.children) {
      // Tags that handle their containers on their own are handled normally.
      if (child.type === 'tag' && containerTagsName.has(child.tag)) {
        transformed.push(
          child.transform(config)
        )
      }
      // All other things, i.e. nodes and prose tags, are inserted into a Prose container, then handled with the prose configuration.
      else {
        transformed.push(
          new Tag(
            'ContainerProse',
            node.transformAttributes(config),
            [ Markdoc.transform(child, proseConfig) ]
          )
        )
      }
    }
    return transformed
  },
}

// Document configuration.
export const config = {
  nodes: {
    document,
  },
  // This is used for the functions validate and transform.
  tags: {
    ...containerConfig.tags,
    ...proseConfig.tags,
  },
}

import Image from 'next/image'

import CardPostHorizontal from '../../components/cards/CardPostHorizontal'
import CardPostVertical from '../../components/cards/CardPostVertical'
import Chapter from '../../components/typos/Chapter'
import ContainerAnnotation from '../../components/containers/ContainerAnnotation'
import ContainerFullWidth from '../../components/containers/ContainerFullWidth'
import ContainerPage from '../../components/containers/ContainerPage'
import ContainerProse from '../../components/containers/ContainerProse'
import Figure from '../../components/typos/Figure'
import FigureCaption from '../../components/typos/FigureCaption'
import Quote from '../../components/typos/Quote'
import QuoteCaption from '../../components/typos/QuoteCaption'
import YoutubeVideo from '../../components/typos/YoutubeVideo'

// Mapping from the name used in the configuration above to the React components.
export const runtimeComponents = {
  components: {
    CardPostHorizontal,
    CardPostVertical,
    Chapter,
    ContainerAnnotation,
    ContainerFullWidth,
    ContainerPage,
    ContainerProse,
    Figure,
    FigureCaption,
    Image,
    Quote,
    QuoteCaption,
    YoutubeVideo,
  },
}

