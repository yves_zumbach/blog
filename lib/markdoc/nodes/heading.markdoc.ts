import { Schema, Tag } from '@markdoc/markdoc'

export function generateTitleId(children, attributes) {
  if (attributes.id && typeof attributes.id === 'string') {
    return attributes.id
  }
  return children
    .filter((child) => typeof child === 'string')
    .join(' ')
    .replace(/[?]/g, '')
    .replace(/\s+/g, '-')
    .toLowerCase()
}

const heading: Schema = {
  children: ['inline'],
  attributes: {
    id: {type: String},
    level: {type: Number, required: true, default: 1},
  },
  transform(node, config) {
    const attributes = node.transformAttributes(config)
    const children = node.transformChildren(config)

    return new Tag(
      `h${node.attributes['level'] + 1}`,
      {
        ...attributes,
        id: generateTitleId(children, attributes),
      },
      children
    )
  },
}

export default heading
