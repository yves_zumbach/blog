import { Tag } from '@markdoc/markdoc'
import sizeOf from 'image-size'

export default {
  description: 'An image.',
  attributes: {
    src: {
      type: String,
      required: true,
    },
    alt: {
      type: String,
      required: true,
    },
    title: {
      type: String,
    },
  },
  transform(node, config) {
    const attributes = node.transformAttributes(config)

    if (attributes.src[0] === '/') {
      const { width, height } = sizeOf('./public' + attributes.src)
      return new Tag(
        'Image',
        {
          ...attributes,
          width,
          height,
        }
      )
    }

    return new Tag(
      'img',
      attributes,
    ) 
  },
}
