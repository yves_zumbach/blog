export interface Image {
  /** The path where the image is stored. */
  path: string
  /** A description of the image. */
  alt: string
  /** The original width of the image. */
  width: number
  /** The original height of the image. */
  height: number
}

export type Breakpoint = 'DEFAULT' | 'sm' | 'md' | 'lg' | 'xl'
export type BreakpointsMaxWidth = Partial<Record<Breakpoint, number>>
export type ElementMaxWidth = ((number) => number) | number
export type ElementMaxWidths = Partial<Record<Breakpoint, ElementMaxWidth>>

export const breakpoints = [
  'DEFAULT',
  'sm',
  'md',
  'lg',
  'xl',
]

const BREAKPOINT_WIDTH_PIXELS: Partial<Record<Breakpoint, [number, number]>> = {
  'DEFAULT': [0, 599],
  'sm': [600, 1023],
  'md': [1024, 1399],
  'lg': [1400, 2047],
  'xl': [2048, undefined],
}

export const WIDTHS_CONTAINER_SM: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: 600,
  md: 768,
}

export const WIDTHS_CONTAINER_MD: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: 1024,
  md: 1024,
}

export const WIDTHS_CONTAINER_LG: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: 1024,
  md: 1400,
  lg: 1400,
}

export const WIDTHS_PROSE: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: 605,
  md: 660,
}

export const WIDTHS_ANNOTATION: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: WIDTHS_PROSE.sm,
  md: 264,
}

export const WIDTHS_PAGE: BreakpointsMaxWidth = {
  DEFAULT: 600,
  sm: WIDTHS_PROSE.sm,
  md: WIDTHS_PROSE.md + WIDTHS_ANNOTATION.md,
}

/**
 * Takes a container max sizes and merge it with the max sizes that an element inside the container can take at various breakpoints.
 */
export function computeBreakpointsMaxWidth(containerMaxWidths: BreakpointsMaxWidth, elementBreakpointsMaxWidth: ElementMaxWidths): BreakpointsMaxWidth {
  let elementMaxWidths = {}
  let lastContainerMaxWidth = null
  let lastElementBreakpointMaxWidth = null
  for (const breakpoint of breakpoints) {
    // Values only need to be computed if the container has a new max width or if the user provided a function for the given breakpoint.
    if (breakpoint in containerMaxWidths || breakpoint in elementBreakpointsMaxWidth) {
      // Update lastContainerMaxWidth if a new container width is available.
      lastContainerMaxWidth = containerMaxWidths[breakpoint] ? containerMaxWidths[breakpoint] : lastContainerMaxWidth
      // Update lastElementBreakpointMaxWidth if a new function is available.
      lastElementBreakpointMaxWidth = elementBreakpointsMaxWidth[breakpoint] ? elementBreakpointsMaxWidth[breakpoint] : lastElementBreakpointMaxWidth
      let elementMaxWidth = null
      if (lastElementBreakpointMaxWidth instanceof Function) {
        elementMaxWidth = lastElementBreakpointMaxWidth(lastContainerMaxWidth)
      } else if (lastElementBreakpointMaxWidth) {
        elementMaxWidth = lastElementBreakpointMaxWidth
      } else {
        elementMaxWidth = lastContainerMaxWidth
      }
      elementMaxWidths[breakpoint] = elementMaxWidth
    }
  }
  return elementMaxWidths
}

/**
 * Takes a container max sizes and merge it with the max sizes that an element inside the container can take at various breakpoints.
 * containerMaxWidths contains the maximum widths that the container can take at various breakpoints.
 * elementMaxWidths is a mapping from a breakpoint to a function that gets as input the max width of the container at that breakpoint and returning the max width of the element at that breakpoint.
 *  If an element takes only half of the width of the container from a given breakpoint on, then the function of this breakpoint should return the width of the container divided by two.
 *  If an element takes a fixed width from a given breakpoint, then the function of this breakpoint should return this dimension directly.
 */
export function computeSizes(containerMaxWidths: BreakpointsMaxWidth, elementMaxWidths?: ElementMaxWidths): string {
  let maxWidths = containerMaxWidths
  if (elementMaxWidths) {
    maxWidths = computeBreakpointsMaxWidth(containerMaxWidths, elementMaxWidths)
  }
  let sizes: string[] = []
  let defaultMaxWidth = null
  for (const breakpoint in maxWidths) {
    const maxWidth = maxWidths[breakpoint]
    const maxWidthString: string = `${Math.round(maxWidth)}px` 
    if (breakpoint === 'DEFAULT') {
      defaultMaxWidth = maxWidthString
    } else {
      sizes.push(`(min-width: ${BREAKPOINT_WIDTH_PIXELS[breakpoint][0]}px) ${maxWidthString}`)
    }
  }
  // Add the default max width last.
  sizes.push(defaultMaxWidth)
  return sizes.join(', ')
}

