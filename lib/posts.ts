import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import sizeOf from 'image-size'
import Markdoc from '@markdoc/markdoc'

import { Image } from './images'
import { assert, imagesDirectory } from './utils'
import { Category, CategoryId, getCategoryIdToCategory } from './categories'

import { config } from './markdoc'

const postsDirectory = path.join(process.cwd(), 'content/posts')

export type PostId = string

export interface Post {
  id: PostId
  title: string
  filepath: string,
  url: string,
  date: string
  metaTitle: string
  abstract: string
  category: Category
  coverImage: Image
  content: string // JSON serialized Markdoc AST, NextJS does not support JS objects in props.
}

type UserDefinedPostMetadata = Omit<Post, 'content' | 'date' | 'filepath' | 'id' | 'metaTitle' | 'url' >

export type PostMetadata = Omit<Post, 'content'>

/**
 * Checks that the `frontmatter` of the post written at `filename` contains all required metadata, then returns a `UserDefinedPostMetadata` object.
 */
function frontmatterToUserDefinedPostMetadata(frontmatter: any, filename: string): UserDefinedPostMetadata {
  assert(frontmatter.title !== undefined, `Missing title metadata in post ${filename}.`)
  assert(frontmatter.abstract !== undefined, `Missing abstract metadata in post ${filename}.`)
  assert(frontmatter.category !== undefined, `Missing category metadata in post ${filename}.`)
  const categoryIdToCategory = getCategoryIdToCategory()
  assert(frontmatter.category in categoryIdToCategory, `Post ${filename} has category ${frontmatter.category} which is not allowed. Please use one of ${categoryIdToCategory.keys()}.`)
  const category = categoryIdToCategory[frontmatter.category]
  assert(frontmatter.coverImage !== undefined, `Missing coverImage metadata in post ${filename}.`)
  const coverImagePath = path.join(imagesDirectory, frontmatter.coverImage)
  const { height, width } = sizeOf(coverImagePath)

  return {
    title: frontmatter.title,
    abstract: frontmatter.abstract,
    category,
    coverImage: {
      path: frontmatter.coverImage,
      width,
      height,
      alt: '',
    },
  }
}

/**
  * Parses the metadata of a post given the name of the file in which the post is stored.
  */
function filenameToPostMetadata(filename: string): PostMetadata {
  // First, check that the file name is valid.
  const filenameRegex = /^\d{4}-\d{2}-\d{2}_[\w_]+\.md(oc)?$/
  if (!filenameRegex.test(filename)) {
    throw `Post with path ${filename} has an invalid file name. Please conform to ${filenameRegex}`
  }

  // Remove ".md" from file name to get id
  const date = filename.substring(0, 10)
  const id = filename.replace(/\.md(oc)?$/, '').substring(11)

  // Read markdown file as string
  const fullPath = path.join(postsDirectory, filename)
  const fileContent = fs.readFileSync(fullPath, 'utf8')

  // Use gray-matter to parse the post's metadata section
  const frontmatter = matter(fileContent).data
  // Ensure all required metadata are present and convert to internal representation.
  const userDefinedPostMetadata = frontmatterToUserDefinedPostMetadata(frontmatter, fullPath)

  let metaTitle = 'metaTitle' in frontmatter ? frontmatter.metaTitle : frontmatter.title

  const postMetadata: PostMetadata = {
    id,
    url: `/posts/${id}`,
    ...userDefinedPostMetadata,
    metaTitle,
    // Using the ISO 8601 representation seems appropriate here.
    date: new Date(date).toISOString(),
    filepath: fullPath,
  }

  return postMetadata
}

/**
 * Returns the metadata of all the posts in the `/posts` folder.
 * The posts will be sorted by descending date.
 */
export function getPostsMetadata() {
  // Get file names under /content/posts
  const filenames = fs.readdirSync(postsDirectory)
  const postsMetadata = filenames.map((filename) => {
    return filenameToPostMetadata(filename)
  })
  
  const postsId = postsMetadata.map(({ id }) => { return id })
  const uniquePostsId = new Set(postsId)
  if (postsId.length !== uniquePostsId.size) {
    throw `Some posts have identical IDs, where IDs are computed as the filename without the date and the extension. This is invalid. Please ensure that all posts' ID are unique. Computed IDs: ${postsId}.`
  }

  // Sort posts by date
  return postsMetadata.sort(({ date: a }, { date: b }) => {
    if (a < b) {
      return 1
    } else if (a > b) {
      return -1
    } else {
      return 0
    }
  })
}

export function getPostMetadataFromId(id: string) {
  const postsMetadata = getPostsMetadata()
  for (const postMetadata of postsMetadata) {
    if (postMetadata.id === id) {
      return postMetadata
    }
  }
  throw `There is no post with ID ${id}. Please use a valid post ID.`
}

export function getPosts() {
  return getPostsMetadata().map((postMetadata) => {
    // Read markdown file as string
    const fileContent = fs.readFileSync(postMetadata.filepath, 'utf8')

    const ast = Markdoc.parse(fileContent)
    const errors = Markdoc.validate(ast, config);
    assert(errors.length == 0, `Post ${postMetadata.id} contains invalid Markdoc syntax. Errors were: ${JSON.stringify(errors)}`)
    const content = Markdoc.transform(ast, config)

    const post: Post = {
      ...postMetadata,
      content: JSON.stringify(content), // We need to stringify the Markdoc AST because NextJS does not support objects in props.
    }

    return post
  })
}

export function getPostFromId(id: string) {
  const posts = getPosts()
  for (const post of posts) {
    if(post.id === id) {
      return post
    }
  }
  throw `There is no post with ID ${id}. Please use a valid post ID.`
}

export function getPostsPath() {
  return getPostsMetadata().map(({ id }) => {
    return { params: { id } }
  })
}

export function getPostsWithCategoryId(categoryId: CategoryId): PostMetadata[] {
  return getPostsMetadata().filter(({ category }) => category.id === categoryId)
}

