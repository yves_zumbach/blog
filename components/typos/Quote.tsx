export default function Quote({
  children,
  caption,
}: {
  children: any,
  caption?: string
}) {
  return <>
    <blockquote className="relative my-12 mx-auto w-fit text-lg italic">
      <img className="absolute -translate-y-3 -translate-x-6 w-8 opacity-30" src="/typo/apostrophe.min.svg" alt="apostrophe" />
      {children}
    </blockquote>
  </>
}

