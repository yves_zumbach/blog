export default function QuoteCaption({
  children,
}: {
  children: any,
}) {
  return <>
    <div className="text-right text-base not-italic">
      {children}
    </div>
  </>
}
