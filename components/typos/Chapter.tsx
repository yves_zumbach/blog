import ContainerAnnotation from '../containers/ContainerAnnotation'
import MinHeightScreenBackground from '../backgrounds/MinHeightScreenBackground'
import SpacerMd from '../spacers/SpacerMdCollapsible'

export default function Chapter({
  title,
  id,
  imagePath,
  imageAlt,
  caption,
}: {
  title: string,
  id: string,
  imagePath: string,
  imageAlt: string,
  caption?: string,
}) {
  return <>
    <div className="md:clear-both">
      <SpacerMd />
      <MinHeightScreenBackground imagePath={imagePath} imageAlt={imageAlt}>
        <div className="font-sans font-semibold text-lg small-caps text-gray-100 opacity-90">
          chapter
        </div>
        <h1 className="font-sans font-bold text-4xl small-caps text-gray-100 opacity-80">
          {title}
        </h1>
      </MinHeightScreenBackground>
      <SpacerMd />
    </div>
  </>
}
