export default function Figure({
  children,
}: {
  children: any,
}) {
  return <>
    <figure>
      {children}
    </figure>
  </>
}
