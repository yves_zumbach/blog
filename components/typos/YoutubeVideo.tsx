export default function YoutubeVideo({
  src,
}: {
  src: string,
}) {
  return <>
    <iframe
      src={src}
      title="YouTube video player"
      className="w-full aspect-video border-0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
    ></iframe>
  </>
}
