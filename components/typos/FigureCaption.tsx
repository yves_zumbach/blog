export default function FigureCaption({
  children,
}: {
  children: any,
}) {
  return <>
    <figcaption className="italic">
      {children}
    </figcaption>
  </>
}
