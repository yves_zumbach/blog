import { useState } from 'react'
import Link from 'next/link'

import ContainerMd from './containers/ContainerMd'

const navItems = [
  {
    text: 'Posts',
    href: '/posts',
  },
  {
    text: 'Shorts',
    href: '/shorts',
  },
  {
    text: 'Portfolio',
    href: '/portfolio',
  },
]

export default function Footer() {
  return <>
    <nav className="bg-gray-950 border-t border-gray-700 pt-8 pb-16">
      <ContainerMd>
        <div className="flex justify-center sm:justify-start">
          <div className="flex flex-col items-center sm:flex sm:flex-row sm:items-baseline sm:items-left gap-6">

            <div className="text-md font-sans small-caps font-semibold text-gray-100 hover:text-gray-10">
              <Link href="/">
                zumbach.dev
              </Link>
            </div>

            {/* Navbar Items */}
            <div className="grid grid-cols-1 justify-between justify-items-center items-baseline gap-x-4 gap-y-4 sm:flex sm:flex-row sm:flex-wrap sm:items-left sm:items-baseline sm:gap-y-2">
              {navItems.map(({ href, text }) =>
                <div key={href} className="w-fit px-2 py-1 rounded font-sans text-sm transition duration-100 hover:text-gray-10 hover:bg-gray-900">
                  <Link href={href}>
                    {text}
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      </ContainerMd>
    </nav>
  </>
}

