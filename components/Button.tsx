export default function Button({ text, href }: { text: string, href: string }) {
  text = text.toLowerCase()
  return <>
    <a href={href} className="px-2 py-1 backdrop-blur-sm shadow-sm border-std rounded font-sans font-semibold text-sm small-caps transition duration-200 hover:shadow-md hover:border-lighter hover:text-gray-100 active:transition-none active:text-gray-300 active:border-std">
      {text}
    </a>
  </>
}
