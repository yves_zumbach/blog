import SpacerLg from '../spacers/SpacerLg'
import SpacerMd from '../spacers/SpacerMd'

export default function Spacer({ }) {
  return <>
    <div className="hidden md:block">
      <SpacerLg />
    </div>
    <div className="md:hidden">
      <SpacerMd />
    </div>
  </>
}
