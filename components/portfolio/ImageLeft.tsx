import Image from 'next/image'

import ContainerPage from '../containers/ContainerPage'

import { assert } from '../../lib/utils'
import { ImageData } from '../../lib/portfolio'

export default function ImageLeft({
    children=null,
    image
}:{
  children?: any,
  image: ImageData
}) {
  assert(!!image, `An image attribute must be provided.`)

  return <>
    <ContainerPage>
      <figure>
        <div className="mx-auto w-full flex flex-col md:flex-row justify-between">
          <div className="flex-none md:w-prose max-w-prose">
            <Image src={image.src} alt={image.alt} />
          </div>
          <div className="flex-none w-full md:w-44">
            <figcaption className="md:!mt-0">
              {image.caption}
            </figcaption>
            {children}
          </div>
        </div>
      </figure>
    </ContainerPage>
  </>
}

