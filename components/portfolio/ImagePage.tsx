import Image from 'next/image'

import ContainerPage from '../containers/ContainerPage'

import { assert } from '../../lib/utils'
import { ImageData } from '../../lib/portfolio'

export default function ImageLeft({
    children=null,
    image
}: {
  children?: any,
  image: ImageData
}) {
  assert(!!image, `An image attribute must be provided.`)

  return <>
    <ContainerPage>
      <figure>
        <Image src={image.src} alt={image.alt} />
        <figcaption>
          {image.caption}
        </figcaption>
      </figure>
    </ContainerPage>
  </>
}

