import Image from 'next/image'

import ContainerPage from '../containers/ContainerPage'
import Spacer from './Spacer'

import { assert } from '../../lib/utils'
import { ImageData } from '../../lib/portfolio'

export default function SideBySide({
  image1,
  image2,
}: {
  children?: any,
  image1: ImageData,
  image2: ImageData,
}) {
  assert(!!image1, `An image attribute must be provided.`)
  assert(!!image2, `An image attribute must be provided.`)

  return <>
    <ContainerPage>
      <div className="mx-auto w-full flex flex-col items-end md:flex-row gap-x-7 justify-between">
        <div className="flex-1">
          <figure>
            <Image src={image1.src} alt={image2.alt} />
            <figcaption>
              {image1.caption}
            </figcaption>
          </figure>
        </div>
        <div className="md:hidden">
          <Spacer />
        </div>
        <div className="flex-1">
          <figure>
            <Image src={image2.src} alt={image2.alt} />
            <figcaption>
              {image2.caption}
            </figcaption>
          </figure>
        </div>
      </div>
    </ContainerPage>
  </>
}
