import Image from 'next/image'

import ContainerPage from '../containers/ContainerPage'

import { ImageData } from '../../lib/portfolio'
import { assert } from '../../lib/utils'

export default function ImageRight({
    children=null,
    image
}: {
  children?: any,
  image: ImageData
}) {
  assert(!!image, `An image attribute must be provided.`)

  return <>
    <ContainerPage>
      <figure>
        <div className="mx-auto w-full flex flex-col-reverse md:flex-row justify-between">
          <div className="flex-none w-full md:w-44">
            <figcaption>
              {image.caption}
            </figcaption>
            {children}
          </div>
          <div className="flex-none md:w-prose max-w-prose">
            <Image src={image.src} alt={image.alt} />
          </div>
        </div>
      </figure>
    </ContainerPage>
  </>
}

