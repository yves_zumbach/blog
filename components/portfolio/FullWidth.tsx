import Image from 'next/image'

import ContainerFullWidth from '../containers/ContainerFullWidth'
import ContainerPage from '../containers/ContainerPage'

import { assert } from '../../lib/utils'
import { ImageData } from '../../lib/portfolio'

export default function FullWidth({
    children=null,
    image
}: {
  children?: any,
  image: ImageData
}) {
  assert(!!image, `An image attribute must be provided.`)

  return <>
    <ContainerFullWidth>
      <figure>
        <Image src={image.src} alt={image.alt} />
        <ContainerPage>
          <figcaption>
            {image.caption}
          </figcaption>
          {children}
        </ContainerPage>
      </figure>
    </ContainerFullWidth>
  </>
}

