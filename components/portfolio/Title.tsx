import ContainerPage from '../containers/ContainerPage'

import { assert } from '../../lib/utils'

export default function ImageLeft({
    children=null,
    text
}) {
  assert(!!text, `A title attribute must be provided.`)

  return <>
    <ContainerPage>
      <h1 className="pb-4 border-b border-gray-800 font-sans font-bold lowercase small-caps tracking-wide text-5xl text-gray-200">
        {text}
      </h1>
      <p className="mt-4 font-sans font-semibold italic text-sm">
        By Yves <span className="small-caps">Zumbach</span>
      </p>
    </ContainerPage>
  </>
}
