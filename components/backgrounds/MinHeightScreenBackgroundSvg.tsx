import { assert } from '../../lib/utils'

import ContainerMd from '../containers/ContainerMd'
import SpacerMd from '../spacers/SpacerMd'

export default function MinHeightScreenBackground({
  children,
  backgroundClasses = 'border-b border-gray-700',
  imagePath,
  imageClasses = '',
  imageOpacity = '20',
}: {
  children: any,
  backgroundClasses?: string,
  imagePath: string,
  imageClasses?: string,
  imageOpacity?: string,
}) {
  assert(!!imagePath, `An imagePath attribute must be provided.`)
  return <>
    <div className={`relative min-h-screen w-full ${backgroundClasses}`}>
      <div className="absolute w-full h-full" style={{contain: 'paint'}}>
          <img className={`sticky top-0 h-screen max-w-none opacity-${imageOpacity} ${imageClasses}`} src={imagePath} alt="Abstract curved lines" />
      </div>
      <div className="relative min-h-screen flex flex-col justify-center">
        <ContainerMd>
          <SpacerMd />
            {children}
          <SpacerMd />
        </ContainerMd>
      </div>
    </div>
  </>
}
