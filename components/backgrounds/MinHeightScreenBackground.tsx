import Image from 'next/image'

import ContainerMd from '../containers/ContainerMd'
import SpacerMd from '../spacers/SpacerMd'

export default function MinHeightScreenBackground({
  children,
  imagePath,
  imageAlt,
  imageClasses = '',
}: {
  children: any,
  imagePath: string,
  imageAlt: string,
  imageClasses?: string,
}) {
  return <>
    <div className="relative min-h-screen w-full bg-gray-900">
      <div className="absolute w-full h-full" style={{contain: 'paint'}}>
        <div className="sticky top-0 w-full h-screen">
          <div className="relative w-full h-screen">
            <Image className={`object-cover object-center max-w-none ${imageClasses}`} src={imagePath} alt={imageAlt} fill sizes="100vh" />
          </div>
        </div>
      </div>
      <div className="absolute z-10 h-full w-full bg-black opacity-50" />
      <div className="relative z-20 min-h-screen flex flex-col justify-center">
        <ContainerMd>
          <SpacerMd />
            {children}
          <SpacerMd />
        </ContainerMd>
      </div>
    </div>
  </>
}
