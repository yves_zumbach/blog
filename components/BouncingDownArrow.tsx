export default function BouncingDownArrow({
  href,
}) {
  return <>
    <div className="absolute bottom-8 left-0 right-0 text-center">
      <a href={href}>
        <div className="animate-bounce">
          <span className="border-b-2 border-r-2 inline-block p-2 rotate-45">
          </span>
        </div>
      </a>
    </div>
  </>
}
