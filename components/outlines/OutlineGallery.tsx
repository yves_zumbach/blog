import Link from 'next/link'

import { assert } from '../../lib/utils'

import Card from '../cards/Card'

export default function OutlineGallery({
    id
}: {
    id: string
}) {
  assert(!!id, `An id property is required, but none was provided.`)
  return (
    <Link href={`/portfolio/${id}`}>
      <div className="group rounded border-b border-gray-700 p-3 leading-tight cursor-pointer backdrop-blur-sm flex items-baseline shadow-sm transition duration-300 hover:shadow-md hover:backdrop-blur-md">
        <div className="grow">
          <div className="font-sans lowercase small-caps font-bold text-xl text-gray-300 leading-7 transition duration-300 group-hover:text-gray-200">
            {id}
          </div>
        </div>
      </div>
    </Link>
  )
}

