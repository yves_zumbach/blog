import Link from 'next/link'

import { assert, dateToYearMonthDay } from '../../lib/utils'
import { Post } from '../../lib/posts'
import { Category } from '../../lib/categories'

import Card from '../cards/Card'

export default function OutlinePost({ post }: { post: Post }) {
  assert(!!post, `A post property is required, but none was provided. Value was: ${post}`)
  const {
    id,
    title,
    abstract,
    date,
    category,
  }: {
    id: string,
    title: string,
    abstract: string,
    date: string,
    category: Category,
  } = post
  return (
    <Link href={`/posts/${id}`}>
      <div className="group rounded border-b border-gray-700 p-3 leading-tight cursor-pointer backdrop-blur-sm flex items-baseline shadow-sm transition duration-300 hover:shadow-md hover:backdrop-blur-md">
        <div className="basis-24 flex-none text-gray-300 transition duration-300 group-hover:text-gray-200">
          {dateToYearMonthDay(date)}
        </div>
        <div className="grow">
          <div className="font-sans lowercase small-caps font-bold text-xl text-gray-300 leading-7 transition duration-300 group-hover:text-gray-200">
            {title}
          </div>
        </div>
        <div className="hidden sm:block flex-none ml-6 text-sm text-gray-300 lowercase transition duration-300 group-hover:text-gray-200">
        {category.name}
        </div>
      </div>
    </Link>
  )
}

