import Link from 'next/link'

import { assert, dateToYearMonthDay } from '../../lib/utils'
import { Short } from '../../lib/shorts'
import { Category } from '../../lib/categories'

import Card from './../cards/Card'

export default function OutlineShort({ short }: { short: Short }) {
  assert(!!short, `A short is required, but none was provided. Value was: ${short}`)
  const {
    id,
    title,
    date,
    category,
    tags
  }: {
    id: string,
    title: string,
    date: string,
    category: Category,
    tags?: string[]
  } = short
  return (
    <Link href={`/shorts/${id}`}>
      <div className="group rounded border-b border-gray-700 p-3 leading-tight cursor-pointer backdrop-blur-sm flex items-baseline shadow-sm transition duration-300 hover:shadow-md hover:backdrop-blur-md">
        <div className="basis-24 flex-none text-gray-300 transition duration-300 group-hover:text-gray-200">
          {dateToYearMonthDay(date)}
        </div>
        <div className="grow font-sans lowercase small-caps font-bold text-lg transition duration-300 group-hover:text-gray-100">
          {title}
        </div>
        <div className="hidden sm:block flex-none ml-6 text-sm text-gray-300 lowercase transition duration-300 group-hover:text-gray-200">
          {category.name}
        </div>
      </div>
    </Link>
  )
}

