'use client'

import { Category } from '../../lib/categories'
import { computeBreakpointsMaxWidth, WIDTHS_CONTAINER_MD } from '../../lib/images'

import CardCategory from '../cards/CardCategory'

export default function CategoriesAll({ children, categories }: { children?: any, categories: Category[] }) {
  return <>
    <div className="grid gap-10 grid-cols-1 sm:grid-cols-2 justify-items-stretch">
      {categories.map((category) => (
        <CardCategory key={category.id} category={category} containerWidths={computeBreakpointsMaxWidth(
          WIDTHS_CONTAINER_MD,
          {
            sm: (w) => w / 2,
            md: (w) => w / 2,
          }
        )} />
      ))}
    </div>
  </>
}


