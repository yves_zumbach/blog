import Link from 'next/link'

import MinHeightScreenBackgroundSvg from '../backgrounds/MinHeightScreenBackgroundSvg'
import OutlineGallery from '../outlines/OutlineGallery'
import SpacerSm from '../spacers/SpacerSm'

const galleries = [
  'portraits',
  'nature',
  'architecture',
]

export default function Portfolio({ }) {
  return <>
    <MinHeightScreenBackgroundSvg
      imagePath="/backgrounds/abstract_lines/1.min.svg"
      imageClasses="ml-auto mr-4 -scale-x-100"
    >
      <h1 className="h1">
        <Link href="/portfolio/">
          Portfolio
        </Link>
      </h1>
      <SpacerSm />

      <div className="flex flex-wrap gap-y-5 md:gap-y-6">
        {galleries.map((id) => (
          <div key={id} className="flex-auto basis-full">
            <OutlineGallery id={id} />
          </div>
        ))}
      </div>
      <SpacerSm />

      {/*
      <div className="flex flex-wrap gap-5 md:gap-6">
        {gallerys.map((gallery) => (
          <div className="flex-auto basis-1/3">
            <CardGallery
              key={gallery.id}
              id={gallery.id}
              image={gallery.image}
              containerWidths={WIDTHS_CONTAINER_MD}
            />
          </div>
        ))}
      </div>
      */}
    </MinHeightScreenBackgroundSvg>
  </>
}

