import Link from 'next/link'

import { assert } from '../../lib/utils'
import { Short } from '../../lib/shorts'

import MinHeightScreenBackgroundSvg from '../backgrounds/MinHeightScreenBackgroundSvg'
import OutlineShort from '../outlines/OutlineShort'
import SpacerSm from '../spacers/SpacerSm'

export default function({
  shorts,
  mostRecentOnly = false,
}: {
  shorts: Short[],
  mostRecentOnly?: boolean,
}) {
  assert(!!shorts, `A shorts attribute must be provided.`)
  if (mostRecentOnly) {
    shorts = shorts.slice(0, 6)
  }
  return <>
    <MinHeightScreenBackgroundSvg
      imagePath="/backgrounds/abstract_lines/4.min.svg"
    >
      <h1 className="h1">
        <Link href="/shorts/">
          Shorts
        </Link>
      </h1>
      <SpacerSm />
      <div className="flex flex-wrap gap-4">
        {shorts.map((short) => (
          <div key={short.id} className="basis-full">
            <OutlineShort short={short} />
          </div>
        ))}
      </div>
    </MinHeightScreenBackgroundSvg>
  </>
}

