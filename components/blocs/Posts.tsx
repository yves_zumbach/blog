import Link from 'next/link'

import { assert } from '../../lib/utils'
import { Post } from '../../lib/posts'

import MinHeightScreenBackgroundSvg from '../backgrounds/MinHeightScreenBackgroundSvg'
import OutlinePost from '../outlines/OutlinePost'
import SpacerSm from '../spacers/SpacerSm'

export default function({
  posts,
  mostRecentOnly = false,
}: {
  posts: Post[],
  mostRecentOnly?: boolean,
}) {
  assert(!!posts, `A posts attribute must be provided.`)
  if (mostRecentOnly) {
    posts = posts.slice(0, 6)
  }
  return <>
    <MinHeightScreenBackgroundSvg
      imagePath="/backgrounds/abstract_lines/5.min.svg"
      imageClasses="ml-auto mr-4"
      >
      <h1 className="h1">
        <Link href="/posts/">
          Posts
        </Link>
      </h1>
      <SpacerSm />
      <div className="flex flex-wrap gap-y-5 md:gap-y-6">
        {posts.map((post) => (
          <div key={post.id} className="flex-auto basis-full">
            <OutlinePost post={post} />
          </div>
        ))}
      </div>
    </MinHeightScreenBackgroundSvg>
  </>
}
