import { WIDTHS_CONTAINER_MD } from '../../lib/images'

import ContainerMd from '../containers/ContainerMd'
import CardNewsletter from '../cards/CardNewsletter'
import SpacerLg from '../spacers/SpacerLg'
import SpacerSm from '../spacers/SpacerSm'

export default function BlocNewsletter() {
  return <>
    <SpacerLg />
    <ContainerMd>
      <h1 className="h1">
        Newsletter
      </h1>
      <SpacerSm />
      <CardNewsletter containerWidths={WIDTHS_CONTAINER_MD} />
    </ContainerMd>
    <SpacerLg />
  </>
}
