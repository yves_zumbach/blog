export default function ContainerInnerMargins({ children, className = '' }) {
  return (
    <div className={`mx-8 sm:mx-10 ${className}`}>
      {children}
    </div>
  )
}
