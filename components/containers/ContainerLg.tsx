import ContainerInnerMargins from './ContainerInnerMargins'

export default function LgContainer({ children }) {
  return (
    <div className="max-w-screen-lg mx-auto w-full">
      <ContainerInnerMargins>
        {children}
      </ContainerInnerMargins>
    </div>
  )
}

