/**
 * When designing margin paragraphs, the first question to answer, is which block should be center aligned on the page.
 * It can be that the main text is center on the page, or it can be that the block containing both the main text and the margin paragraphs is centered.
 * I decided to center the block containing both the main text and the margin paragraphs, because I think I will use margin paragraphs a lot, so it makes more sense to center both the text and the margin paragraphs.
 */

import ContainerInnerMargins from './ContainerInnerMargins'

export default function ContainerPage({ children }) {
  return <>
    <ContainerInnerMargins>
      <div className="mx-auto w-full max-w-prose md:w-page md:max-w-none hyphens-auto">
        {children}
      </div>
    </ContainerInnerMargins>
  </>
}
