import ContainerInnerMargins from './ContainerInnerMargins'

export default function SmContainer({ children }) {
  return (
    <div className="mx-auto max-w-screen-sm md:max-w-[768px] w-full">
      <ContainerInnerMargins>
        {children}
      </ContainerInnerMargins>
    </div>
  )
}
