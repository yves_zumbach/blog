import ContainerInnerMargins from './ContainerInnerMargins'

export default function MdContainer({ children }) {
  return (
    <div className="max-w-screen-md mx-auto w-full">
      <ContainerInnerMargins>
        {children}
      </ContainerInnerMargins>
    </div>
  )
}

