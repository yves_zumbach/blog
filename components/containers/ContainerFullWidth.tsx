export default function ContainerFullWidth({ children }) {
  return <>
    <div className="w-full hyphens-auto md:clear-both">
      {children}
    </div>
  </>
}
