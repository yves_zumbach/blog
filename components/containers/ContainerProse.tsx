import ContainerPage from './ContainerPage'

export default function ContainerProse({ children }) {
  return <>
    <ContainerPage>
      <div className="md:ml-annotation md:max-w-prose">
        {children}
      </div>
    </ContainerPage>
  </>
}
