import ContainerPage from './ContainerPage'

export default function ContainerAnnotation({ children }) {
  return <>
    <ContainerPage>
      <aside className="md:float-left md:clear-left md:w-44">
        {children}
      </aside>
    </ContainerPage>
  </>
}
