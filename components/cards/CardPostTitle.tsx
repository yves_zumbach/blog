export default function CardTitle({ title }) {
  return (
    <div className="pb-4 font-sans lowercase small-caps font-bold text-2xl text-gray-300 leading-7 transition duration-300 group-hover:text-gray-200">
      {title}
    </div>
  )
}

