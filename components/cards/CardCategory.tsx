import Link from 'next/link'
import Image from 'next/image'

import { assert } from '../../lib/utils'
import { Category } from '../../lib/categories'
import { BreakpointsMaxWidth, computeSizes } from '../../lib/images'

import Card from './Card'

export default function CategoryCard({
  category,
  containerWidths,
  small=false
}: {
  category: Category,
  containerWidths: BreakpointsMaxWidth,
  small?: boolean
}) {
  assert(!!category, `A category property is required, but none was provided. Value was: ${category}`)
  assert(!!containerWidths, `A containerWidths property is required, but none was provided. Value was: ${containerWidths}`)
  const {
    name,
    iconPath,
    image,
    description
  }: Category = category
  return <>
    {/*
      The code hereafter makes it possible to guarantee that category cards will be vertical or square but never horizontal.
      Further, it guarantees the alignment of the card inside the parent.
    */}
    <Card noBorder={true} cardClasses={`card-category ${small ? 'h-40' : 'h-80 sm:h-72' } relative`}>
      <Link href={`categories/${category.id}`}>
        <Image className="absolute object-cover" src={image.path} alt={`${name} category.`} fill sizes={computeSizes(containerWidths)} />
        <div className="absolute h-full w-full bg-gradient-to-t from-gray-950" />
        <div className="relative h-full w-full p-6 flex flex-col justify-end">
          <div className="mx-auto mb-8 sm:mb-6">
            <img src={iconPath} className={`${ small ? 'h-10' : 'h-16' } opacity-70 transition duration-300 group-hover:opacity-90`} />
          </div>
          <div className="mx-auto italic text-sm text-gray-10 opacity-70 transition duration-300 group-hover:opacity-90">
            Category
          </div>
          <div className="mx-auto mt-1 mb-2 sm:mb-0">
            <div className={`text-center leading-5 font-sans lowercase small-caps font-semibold text-gray-50 ${ !small && 'text-xl' } opacity-70 transition duration-300 group-hover:opacity-90`}>
              {name}
            </div>
          </div>
        </div>
      </Link>
    </Card>
  </>
}


