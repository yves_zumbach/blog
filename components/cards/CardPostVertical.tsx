import Image from 'next/image'
import Link from 'next/link'

import { assert } from '../../lib/utils'
import { BreakpointsMaxWidth, computeSizes, Image as ImageClass } from '../../lib/images'
import { PostMetadata } from '../../lib/posts'

import Card from './Card'
import CardPostTitle from './CardPostTitle'

export default function CardPostVertical({
  post,
  containerWidths
}: {
  post: PostMetadata,
  containerWidths: BreakpointsMaxWidth
}) {
  assert(!!post, `A post property is required, but none was provided. Value was: ${post}`)
  assert(!!containerWidths, `A containerWidths property is required, but none was provided. Value was: ${containerWidths}`)
  const {
    id,
    title,
    abstract,
    coverImage,
  }: {
    id: string,
    title: string,
    abstract: string,
    coverImage: ImageClass,
  } = post
  return (
    <Card>
      <div className="card-post">
        <Link href={`/posts/${id}`}>
          <div className="cursor-pointer">
            <div className="relative h-60">
              <Image className="object-cover img grayscale-std" src={coverImage.path} alt={coverImage.alt} fill sizes={computeSizes(containerWidths)} />
            </div>
            <div className="p-6">
              <CardPostTitle title={title} />
              <div className="transition duration-300 group-hover:text-gray-100">
                {abstract}
              </div>
            </div>
          </div>
        </Link>
      </div>
    </Card>
  )
}

