export default function Card({
  children,
  noBorder = false,
  noHoverEffect = false,
  cardClasses = ''
}) {
return (
  <div className={`group overflow-hidden rounded-lg ${noBorder ? '' : 'border-std'} backdrop-blur-sm shadow-md card ${cardClasses} transition duration-300 ${noHoverEffect ? '' : 'hover:shadow-xl hover:backdrop-blur'}`}>
    {children}
  </div>
  );
}

