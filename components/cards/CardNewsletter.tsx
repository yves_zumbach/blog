import Image from 'next/image'

import { BreakpointsMaxWidth, computeSizes } from '../../lib/images'

import Card from './Card'
import Button from '../Button'

export default function CardNewsletter({ containerWidths }: { containerWidths: BreakpointsMaxWidth }) {
  return (
    <>
      <Card cardClasses="relative" noHoverEffect={true}>
        <div className="absolute w-full h-full bg-repeat opacity-20" style={{backgroundImage: 'url(/backgrounds/topography/1.min.svg)', backgroundSize: '500px'}} />
        <div className="relative p-6">
          <div className="font-sans text-2xl font-semibold lowercase small-caps">
            Want to hear about new contents?
          </div>
          <div className="">
            Subscribe to my newsletter, free forever, no ads, unsubscribe anytime.
          </div>

          <div
            className="mt-6"
          >
            <form
              method="post"
              action="https://newsletter.infomaniak.com/external/submit"
              target="_blank"
            >
              <div className="flex flex-wrap justify-center gap-3">
                <input
                  type="hidden"
                  name="key"
                  value="eyJpdiI6IkpSU0ZxclwvWDREamw3MkhBS3FvM20wREwzc1lCSkFsRUErTEc1N3FmSVwvND0iLCJ2YWx1ZSI6InhOME9GdjBydUhRc25POEdoQUJZc09Ua2p5VThcLys4WElaSkdzRDIycktJPSIsIm1hYyI6ImUzODFjZGMzMjk3MjZmM2I1ZDA4MzQ0ZTQ2NTAxMTE4ZmZlNWQzZTk0MDA2NTRmNjU4MWNkOTBjMWJjNjM3MWUifQ=="
                />
                <input
                  type="hidden"
                  name="webform_id"
                  value="13176"
                />
                <input
                  type="email"
                  name="inf[1]"
                  data-inf-meta="1"
                  data-inf-error="Please give an email address"
                  required={true}
                  placeholder="Email"
                  className="min-w-48 max-w-72 w-full bg-transparent backdrop-blur-sm py-1 px-3 rounded-sm border-b-2 border-gray-600 transition duration-200 focus:outline-none focus:border-gray-400 focus:backdrop-blur-md focus:shadow-sm"
                />

                <input
                  type="submit"
                  name=""
                  value="subscribe →"
                  className="cursor-pointer px-2 py-1 backdrop-blur-sm shadow-sm border-std rounded font-sans font-semibold text-sm lowercase small-caps transition duration-200 hover:shadow-md hover:border-lighter hover:text-gray-100 active:transition-none active:text-gray-300 active:border-std"
                />
              </div>
            </form>
          </div>
        </div>
      </Card>
    </>
  )
}
