import Image from 'next/image'
import Link from 'next/link'

import { assert, dateToYearMonthDay } from '../../lib/utils'
import { BreakpointsMaxWidth, computeSizes } from '../../lib/images'
import { PostMetadata } from '../../lib/posts'

import Card from './Card'
import CardPostTitle from './CardPostTitle'

export default function CardPostHorizontal({
  post,
  containerWidths
}: {
  post: PostMetadata,
  containerWidths: BreakpointsMaxWidth
}) {
  assert(!!post, `A post property is required, but none was provided. Value was: ${post}`)
  assert(!!containerWidths, `A containerWidths property is required, but none was provided. Value was: ${containerWidths}`)
  const {
    id,
    title,
    abstract,
    coverImage,
  }: {
    id: string,
    title: string,
    abstract: string,
    coverImage: any,
  } = post
  return (
    <Card>
      <div className="card-post">
        <Link href={`/posts/${id}`}>
          <div className="sm:flex cursor-pointer">
            <div className="relative h-60 sm:w-1/2 sm:min-h-64 sm:h-auto lg:w-3/5">
              <Image className="object-cover img grayscale-std" src={coverImage.path} alt={coverImage.alt} fill sizes={computeSizes(containerWidths, {
                sm: (containerWidth) => containerWidth / 2,
                lg: (containerWidth) => 3 * containerWidth / 5,
              })} />
            </div>
            <div className="p-6 sm:w-1/2 lg:w-2/5">
              <CardPostTitle title={title} />
              <div className="transition duration-300 group-hover:text-gray-100">
                {abstract}
              </div>
            </div>
          </div>
        </Link>
      </div>
    </Card>
  )
}
