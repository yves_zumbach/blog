# Containers

I created three containers to hold content taking various horizontal space: a small one, a medium one and a large one.

The small container's width can be no larger than the width of the small breakpoint, i.e. 600px.
The width of the medium container can be no more than 1024px, which is also the width of the `md` breakpoint.
Finally, the large container can be no larger than 1600px, which is the width of the large breakpoint.

Following is a graphical representation of the containers that can be differentiated at various breakpoints:

DEFAULT-sm: sm md lg
sm-md:      sm/md lg
md-lg:      sm/md/lg
lg-xl:      sm/md/lg
xl-infty:   sm/md/lg

