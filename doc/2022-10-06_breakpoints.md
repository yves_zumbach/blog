# CSS Breakpoints

There is a tradeoff between completeness of the breakpoints and complexity (to understand and remember what the breakpoints represent, but also in terms of development speed if you want to cover all the breakpoints).

Tailwind proposes the following default:

- `DEFAULT`: Mobile portrait: less than 640px
- `sm`: Mobile landscape: > 640px
- `md`: Tablet portrait: > 768px
- `lg`: Tablet landscape: > 1024px
- `xl`: Laptop: > 1280px
- `2xl`: Desktops: > 1536px

Deviating from Tailwind's default has a cost.
It makes it impossible to "just" copy-paste code from the web and requires manual modifications.

Nevertheless, there are too many breakpoints in my opinion and their names, while short, do not represent well what they target for.
So the decision is hard to take.

I decided to use the following breakpoints, taken from https://stackoverflow.com/questions/49293327/what-is-the-standard-css-container-size-in-2018:

```js
// Anything smaller than sm is probably a portrait mobile.
sm: '600px', // mobile landscape, tablet (portrait)
md: '1024px', // chromebooks, ipad pro (portrait), tablet (landscape)
lg: '1600px', // most laptops and desktops
xl: '2048px' // 2k and up
```

Names are identical to Tailwind's, which is confusing, but enables copy-pasting code.
The breakpoints make a lot of sense for me.

