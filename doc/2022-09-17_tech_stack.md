# Tech Stack

## Web Framework

Static website using Next.js
Including statically generated markdown blog posts.

Markdown renderer: markdoc.
Enables extending the markdown syntax with custom tags.
Tags can include NextJS components.
Provided by Stripe, the internet payment framework: strong longevity guarantees.
Open Source, under MIT Licence.

TailwindCSS for CSS: I know the framework, it is very versatile, I have some previous blogs written with it to get some inspiration from, etc.

## Hosting

Vercel: edge deploys, previews ,integrates with GitLab, enables dynamic websites if needs be, builds the production website on their servers, so no need for too much CI/CD.

