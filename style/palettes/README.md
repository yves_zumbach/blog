# Palettes

Palettes are defined using https://palettte.app/.
They can be imported from and exported to the online tool for easy visual modifications.

The script in this folder can be used to transform the exported JSON palette to various output formats.
To run the script, use `node ./convert_palette.js <json_palette_file>`.

