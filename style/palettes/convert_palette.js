const fs = require('fs')

const args = process.argv.slice(2)

fs.readFile(args[0], 'utf8', (err, data) => {
  if (err) {
    console.log(err)
    return
  }
  const jsonData = JSON.parse(data)
  const palette = jsonData[0]['swatches']
  palette.forEach(swatch => {
    console.log(`        "${swatch.name}": "#${swatch.color}",`)
  })
})
