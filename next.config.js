const withMarkdoc = require('@markdoc/next.js')

/** @type {import('next').NextConfig} */
const nextConfig = withMarkdoc({ mode: 'static', schemaPath: './lib/markdoc/' })({
  i18n: {
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: 'en-US',
    // These are all the locales you want to support in
    // your application
    locales: ['en-US', ],
  },
  pageExtensions: ['js', 'jsx', 'ts', 'tsx', 'md', 'mdoc'],
  // image-size, a JS package for reading image size that I use to extract at build time the size of images, requires the `fs` nodejs module (to read the image files).
  // For some reason, the code below is required to make fs available.
  // Found at https://stackoverflow.com/questions/64926174/module-not-found-cant-resolve-fs-in-next-js-application
  webpack(config) {
    config.resolve.fallback = {
      ...config.resolve.fallback,
      fs: false,
    };
    return config;
  },
})

module.exports = nextConfig
