/** @type {import('tailwindcss').Config} */

const colors = require('tailwindcss/colors')

const pageLengths = {
  prose: '55ch',
  annotation: '13rem',
  page: `calc(55ch + 13rem)`,
}

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: 'class',
  theme: {
    boxShadow: {
      // x-shift y-shift [blur-radius [extend]] color
      'sm': [
        '0 2px 4px 2px rgb(0, 0, 0, 0.8)',
        '0 1px 2px -1px rgb(0, 0, 0, 0.8)',
      ],
      DEFAULT: [
        '0 4px 8px 2px rgb(0, 0, 0, 0.8)',
        '0 2px 3px -2px rgb(0, 0, 0, 0.8)',
      ],
      'md': [
        '0 10px 12px -0px rgb(0, 0, 0, 0.8)',
        '0 4px 4px 0px rgb(0, 0, 0, 0.8)',
      ],
      'lg': [
        '0 15px 20px -0px rgb(0, 0, 0, 0.8)',
        '0 8px 8px 0px rgb(0, 0, 0, 0.8)',
      ],
      'xl': [
        '0 20px 25px -0px rgb(0, 0, 0, 0.9)',
        '0 12px 12px 0px rgb(0, 0, 0, 0.9)',
      ],
      '2xl': [
        '0 25px 25px -5px rgb(0, 0, 0, 1)',
        '0 14px 14px -3px rgb(0, 0, 0, 1)',
      ],
    },
    colors: {
      transparent: 'transparent',
      // current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: {
        "990": "#030304",
        "980": "#050507",
        "970": "#09090B",
        "960": "#0C0C0F",
        "950": "#0E0E12",
        "940": "#101016",
        "930": "#12121A",
        "920": "#14131F",
        "900": "#141326",
        "800": "#1E1C3C",
        "700": "#33305B",
        "600": "#4F4C7B",
        "500": "#6D6A98",
        "400": "#8A87AD",
        "300": "#A4A2C1",
        "200": "#C1BFD6",
        "100": "#DCDBEA",
        "50": "#EEEEF5",
        "10": "#FCFCFD",
      },
      red: colors.red,
      orange: colors.orange,
      yelow: colors.yelow,
      green: colors.green,
      blue: colors.blue,
      purple: colors.purple,
      fuchsia: colors.fuchsia,
      pink: colors.pink,
      rose: colors.rose,
    },
    fontFamily: {
      // Font names are wrapped in double quotes to escape special characters like spaces in CSS.
      'sans': ['var(--font-source-sans)', 'sans-serif'],
      'serif': ['var(--font-source-serif)', 'serif'],
      'mono': ['var(--font-source-code)', 'monospace'],
    },
    screens: {
      // DEFAULT: Phone portrait
      sm: '600px', // mobile landscape, tablet (portrait)
      md: '1200px', // chromebooks, ipad pro (portrait), tablet (landscape)
      lg: '1400px', // most laptops and desktops
      xl: '2048px' // 2k and up
    },
    extend: {
      fontSize: {
        '3xs': '0.5rem',
        '2xs': '0.625rem',
      },
      grayscale: {
        'std': '95%',
        '50': '50%',
      },
      height: {
        auto: 'auto',
      },
      margin: {
        ...pageLengths,
      },
      maxWidth: ({ theme }) => ({
        '3xs': '12rem',
        '2xs': '16rem',
        ...pageLengths,
        ...theme('spacing'),
      }),
      minHeight: {
        '64': '16rem',
        '80': '20rem',
      },
      minWidth: ({ theme }) => ({
        ...theme('spacing'),
      }),
      width: {
        ...pageLengths,
      },
    },
  },
  plugins: [],
}
